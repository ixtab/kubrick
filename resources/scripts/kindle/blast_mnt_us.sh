#!/bin/sh -e

# since we have determined beforehand that all our expectations about the
# system state actually hold (see check_blast_mnt_us.sh), nothing here
# is expected to fail.

/bin/umount /mnt/us/
/bin/umount /mnt/base-us/

/sbin/mkdosfs -F 32 -n "Kindle" /dev/loop/0

/bin/mount /mnt/base-us/
/bin/mount /mnt/us/
