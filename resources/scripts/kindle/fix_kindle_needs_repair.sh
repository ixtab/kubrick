#!/bin/sh -e

# K4
/usr/bin/test -f /var/local/system/.framework_reboots && /bin/rm -f /var/local/system/.framework_reboots

# K5
/usr/bin/test -f /var/local/upstart/lab126_gui.restarts && /bin/rm -f /var/local/upstart/lab126_gui.restarts

exit 0
