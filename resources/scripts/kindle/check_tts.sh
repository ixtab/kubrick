#!/bin/sh -e

# actually 254880, but we keep a safety margin of 10%
REQUIRED_TTS=280368

if [ `/bin/mount | /bin/grep -c /mnt/us` != 1 ]; then
	echo "FAIL: /mnt/us not mounted"
	exit 0;
fi


avail=`/bin/df | /bin/grep '/mnt/us$' | awk '{print $4}'`

used_tts=0
if [ -e /mnt/us/tts ]; then
	used_tts=`/usr/bin/du /mnt/us/tts | /usr/bin/tail -n 1 | /usr/bin/awk '{print $1}'`
fi

#echo "avail: $avail used $used_tts required $REQUIRED_TTS"
target=$(( $avail + $used_tts - $REQUIRED_TTS ))
#echo "target: $target"

if [ $target -gt 0 ]; then
	echo "OK: TTS can be installed";
	exit 0;
else
	echo "FAIL: TTS installation impossible - not enough space"
	exit 0;
fi
