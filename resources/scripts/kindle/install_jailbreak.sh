#!/bin/sh -e

(/bin/mount|/bin/grep /dev/mmcblk0p1) && /bin/umount /dev/mmcblk0p1
(/bin/mount|/bin/grep /dev/mmcblk0p1) && exit 1

/bin/mount -o rw /dev/mmcblk0p1 /mnt/mmc

mkdir -p /mnt/mmc/etc/uks/
cat > "/mnt/mmc/etc/uks/pubdevkey01.pem" << EOF
-----BEGIN PUBLIC KEY-----
MIGfMA0GCSqGSIb3DQEBAQUAA4GNADCBiQKBgQDJn1jWU+xxVv/eRKfCPR9e47lP
WN2rH33z9QbfnqmCxBRLP6mMjGy6APyycQXg3nPi5fcb75alZo+Oh012HpMe9Lnp
eEgloIdm1E4LOsyrz4kttQtGRlzCErmBGt6+cAVEV86y2phOJ3mLk0Ek9UQXbIUf
rvyJnS2MKLG2cczjlQIDAQAB
-----END PUBLIC KEY-----
EOF

/bin/umount /dev/mmcblk0p1
