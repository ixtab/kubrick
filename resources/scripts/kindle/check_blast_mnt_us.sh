#!/bin/sh -e

if [ `/bin/mount | /bin/grep -c /mnt/us` != 1 ]; then
	echo "FAIL: /mnt/us not mounted"
	exit 0;
fi

if [ `/bin/mount | /bin/grep /mnt/base-us | /bin/grep -c '^/dev/loop/0'` != 1 ]; then
	echo "FAIL: /mnt/base-us not mounted, or mounted from unexpected location"
	exit 0;
fi

echo "OK: user partition can be reformatted";
