#!/bin/sh -e

# This script blasts (reformats) the /mnt/us partition on the Kindle, i.e., the partition where
# the user's books etc. are stored. (For the Kindle Touch, this will also remove the TTS directory,
# so it makes sense to also make them reinstall TTS by default if this is done).
#
# This script will only be run by Kubrick if it is reasonably safe to do so. The script that is
# run on the Kindle will also remount all necessary partitions after it is done.
#
# Like all other scripts, this script only works correctly when invoked from the kubrick main directory.

. resources/scripts/local/constants
scp -i $SSH_KEY resources/scripts/kindle/blast_mnt_us.sh root@$KINDLE_IP:/tmp/
ssh -i $SSH_KEY root@$KINDLE_IP '/bin/sh /tmp/blast_mnt_us.sh'

