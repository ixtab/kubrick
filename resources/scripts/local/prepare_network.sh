#!/bin/sh -e
# This script configures the local network connection.
# Like all other scripts, this script only works correctly when invoked from the kubrick main directory.


if [ "x$1" = "x" ]; then
	echo "missing argument: <network-interface>"
	exit 1;
fi

IFACE=$1;

. ./resources/scripts/local/constants

ifconfig $IFACE $LOCAL_IP netmask 255.255.255.0
sleep 3
ping -c 1 $KINDLE_IP || exit 1
