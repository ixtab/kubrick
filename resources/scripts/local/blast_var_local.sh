#!/bin/sh -e

# This script blasts the /var/local (/dev/mmcblk0p3) partition on the Kindle.
# Like all other scripts, this script only works correctly when invoked from the kubrick main directory.

. resources/scripts/local/constants
scp -i $SSH_KEY resources/scripts/kindle/blast_var_local.sh root@$KINDLE_IP:/tmp/
ssh -i $SSH_KEY root@$KINDLE_IP '/bin/sh /tmp/blast_var_local.sh'

