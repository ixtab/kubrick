#!/bin/sh -e

# This script checks whether enough space is available to install TTS files.
# Like all other scripts, this script only works correctly when invoked from the kubrick main directory.

. resources/scripts/local/constants

if [ -f resources/devices/k5/tts.tar.lzma ]; then
	scp -i $SSH_KEY resources/scripts/kindle/check_tts.sh root@$KINDLE_IP:/tmp/
	ssh -i $SSH_KEY root@$KINDLE_IP '/bin/sh /tmp/check_tts.sh'
else
	echo "FAIL: TTS file not found"
fi
