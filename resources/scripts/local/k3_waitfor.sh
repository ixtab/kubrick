#!/bin/sh -e

# This script waits for a K3 Kindle to show up in USB downloader mode.
# Like all other scripts, this script only works correctly when invoked from the kubrick main directory.


#./k3flasher resources/ramkernel/mx35to2_mmc.bin info || exit 1
# Bah. k3flasher exits with a non-zero exit code even on success... >:-(
./k3flasher resources/ramkernel/mx35to2_mmc.bin info 2>&1 | grep "DO NOT MAKE THE FOLLOWING INFORMATION PUBLIC"
exit 0
