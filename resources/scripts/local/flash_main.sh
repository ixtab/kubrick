#!/bin/sh -e

# This script installs the main partition on the Kindle.
# Like all other scripts, this script only works correctly when invoked from the kubrick main directory.

if [ "x$1" = "x" ]; then
	echo "missing argument: <device-id>"
	exit 1;
fi

KINDLE_ID=$1;

. resources/scripts/local/constants

SFS_DIR=/tmp/kubrick_sfs

(/bin/mount|/bin/grep $SFS_DIR) && /bin/umount $SFS_DIR
(/bin/mount|/bin/grep $SFS_DIR) && exit 1

mkdir -p $SFS_DIR || exit 1
mount -o loop -t squashfs resources/devices/$KINDLE_ID/fastboot.sfs $SFS_DIR || exit 1


scp -i $SSH_KEY resources/scripts/kindle/umount_main.sh root@$KINDLE_IP:/tmp/
ssh -i $SSH_KEY root@$KINDLE_IP '/bin/sh /tmp/umount_main.sh'

cat $SFS_DIR/main_kernel | ssh -i $SSH_KEY root@$KINDLE_IP '/bin/dd bs=4096 of=/dev/mmcblk0 seek=65'
cat resources/devices/$KINDLE_ID/main_partition.tar.lzma | ssh -i $SSH_KEY root@$KINDLE_IP '/bin/tar xaOf - | /bin/dd bs=4096 of=/dev/mmcblk0p1'

umount $SFS_DIR 2>/dev/null || exit 0
exit 0;
