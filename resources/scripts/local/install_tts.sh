#!/bin/sh -e

# This script installs the TTS files on a Kindle Touch.
# Like all other scripts, this script only works correctly when invoked from the kubrick main directory.

. resources/scripts/local/constants
ssh -i $SSH_KEY root@$KINDLE_IP '/bin/rm -rf /mnt/us/tts'

cat resources/devices/k5/tts.tar.lzma | ssh -i $SSH_KEY root@$KINDLE_IP '/bin/tar xavf - -C /mnt/us/'

