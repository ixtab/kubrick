#!/bin/sh -e

# This script fixes the "Your Kindle needs repair" screen.
# Like all other scripts, this script only works correctly when invoked from the kubrick main directory.

. resources/scripts/local/constants
scp -i $SSH_KEY resources/scripts/kindle/fix_kindle_needs_repair.sh root@$KINDLE_IP:/tmp/
ssh -i $SSH_KEY root@$KINDLE_IP '/bin/sh /tmp/fix_kindle_needs_repair.sh'

