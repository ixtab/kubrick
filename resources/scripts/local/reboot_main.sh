#!/bin/sh

# This script reboots the Kindle to main mode.
# Like all other scripts, this script only works correctly when invoked from the kubrick main directory.

. resources/scripts/local/constants
scp -i $SSH_KEY resources/scripts/kindle/reboot_main.sh root@$KINDLE_IP:/tmp/
ssh -i $SSH_KEY root@$KINDLE_IP '/bin/sh /tmp/reboot_main.sh'

exit 0
