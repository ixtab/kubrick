#!/bin/sh -e
if [ "x$1" = "x" ]; then
	echo "Parameter required: device";
	exit 1;
fi
KD=$1
if [ ! -d "resources/devices/$KD" ]; then
	echo "Invalid device: $KD";
	exit 2;
fi

SFS_DIR=/tmp/kubrick_sfs

(/bin/mount|/bin/grep $SFS_DIR) && /bin/umount $SFS_DIR
(/bin/mount|/bin/grep $SFS_DIR) && exit 1

mkdir -p $SFS_DIR || exit 1
mount -o loop -t squashfs resources/devices/$KD/fastboot.sfs $SFS_DIR || exit 1

./fastboot flash diags_kernel $SFS_DIR/diags_kernel
./fastboot flash diags $SFS_DIR/diags_partition
./fastboot setvar bootmode diags
./fastboot reboot

umount $SFS_DIR 2>/dev/null || exit 0
exit 0;

