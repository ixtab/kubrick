#!/bin/sh -e

# This script checks whether we can "blast" (reformat) /mnt/us.
# Under all normal conditions, this should return "OK".
# Like all other scripts, this script only works correctly when invoked from the kubrick main directory.

. resources/scripts/local/constants

scp -i $SSH_KEY resources/scripts/kindle/check_blast_mnt_us.sh root@$KINDLE_IP:/tmp/
ssh -i $SSH_KEY root@$KINDLE_IP '/bin/sh /tmp/check_blast_mnt_us.sh'
