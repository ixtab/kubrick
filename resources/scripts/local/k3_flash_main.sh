#!/bin/sh -e

# This script flashes the main partition on a K3
# Like all other scripts, this script only works correctly when invoked from the kubrick main directory.

SFS_DIR=/tmp/kubrick_sfs

(/bin/mount|/bin/grep $SFS_DIR) && /bin/umount $SFS_DIR
(/bin/mount|/bin/grep $SFS_DIR) && exit 1

mkdir -p $SFS_DIR || exit 1
mount -o loop -t squashfs resources/devices/k3/main_partition.sfs $SFS_DIR || exit 1

MP=$SFS_DIR/main_partition

if [ ! -f $MP ]; then
	echo "Whoops, unexpected error: $MP does not exist"
	exit 1
fi

./k3flasher resources/ramkernel/mx35to2_mmc.bin program rootfs $MP 2>&1

umount $SFS_DIR 2>/dev/null || exit 0
exit 0;

