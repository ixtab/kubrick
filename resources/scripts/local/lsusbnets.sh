#!/bin/sh
# this file lists all currently available USB network interfaces by name (independent of whether they're up or down).
ifconfig -a|egrep '^usb'|awk '{print $1;}'
