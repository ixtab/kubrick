Kubrick
=======

Kubrick is a Live System to debrick Kindle devices.
It should be working on pretty much every PC or laptop out there
which can boot from a CD, DVD, or USB drive.

The Live system will not make any changes to your installed OS,
and for the average end user, it will simply provide a dialog-
based wizard that guides through the entire debricking procedure.

Questions and Answers:
----------------------

Q:  How do I prepare it for use?
A1: If you downloaded a CD or DVD image:
    Burn boot.iso to a CD or DVD. How to do this depends on your
    OS and installed software. If in doubt, ask your favorite
    search engine.
A2: If want to make a bootable USB Stick:
    BEFORE YOU START: The following procedure ***WILL DELETE ALL EXISTING
                      DATA FROM THE USB STICK*** - You have been warned.
    STEP 1:
        - Download and unzip kubrick-usb-base.zip. This is a very
          small file which provides a basic 1 GB USB filesystem that can boot
          a Kubrick image on many computers. However, it does *NOT* contain an
          actual Kubrick system yet. See below (step 2) for more.
        - Write kubrick-usb-base.img to your USB stick. How to do this depends
          on your OS and installed Software, but here are a few hints:
          * On Windows, you can use http://sourceforge.net/projects/win32diskimager/
          * On Linux or MacOS, you can use the command line, for instance:
            sudo dd if=kubrick.img of=USB_DEVICE. Look up the documentation.
    STEP 2:
        - Download and unzip an ISO (CD or DVD) image which supports the
          Kindle(s) that you want to debrick. 
        - Copy the contained "boot.iso" file to the "efi/boot/" folder on the
          Kubrick USB stick. Do not modify any other files on the USB stick.
        - Safely eject the USB stick. The USB stick is now prepared to boot
          and debrick the devices you selected.

Q: After the preparation - how do I use it?
A: Insert the CD, DVD, or USB stick into your computer.
   Make the computer boot from the Kubrick device.
   Again, the exact procedure really depends on your hardware, so
   I can't give any more specific instructions. Most computers
   allow you to choose the boot device using a function key,
   like F8, F9, F10 or F12.
   - On older computers, you may have to enter the BIOS to manually select
     the boot order.
   - On Apple computers, press and hold the left "alt" ("option") key while
     turning on the computer. The CD/USB will show up with a CD icon labelled
     "Windows", and the USB stick with a USB disk icon labelled "EFI Boot".
     See below for more Apple notes.

Q: What do I do on the boot screen?
A: You generally do not need to do anything. Just wait for a few seconds.

Q: Ok, it booted - what now?
A: The debrick wizard starts automatically.
   The only keys you really need from there on are the arrow keys,
   and the ENTER key. Follow the instructions on the screen.

Q: I'm using an Apple computer with the USB stick, and it won't boot.
A: Welcome to the Apple universe. Most modern Macs actually can boot
   from that USB stick -- but only if you also install Boot Camp.
   Try booting from a CD or DVD. If that doesn't work -- well,
   see the "think different" credo in action.

Q: The kubrick-usb-base.img file actually works, but now my USB stick
   only shows up as 1GB, instead of its actual capacity of 4/16/X GB.
A: Yes, that is normal.  Use a tool that allows to (non-destructively)
   resize partitions. An example would be GParted:
   http://gparted.sourceforge.net/

Q:  I'm an advanced user, and I want to boot Kubrick with custom kernel
    parameters. How can I...?
A1: When booting from a CD or DVD, edit the boot prompt, or follow the
    isolinux instructions.
A2: When booting from a USB stick on standard x86 hardware, use GRUB's
    built-in mechanisms to modify boot behavior. Or, edit the
    boot/grub/grub.cfg file on the USB stick.
A3: When booting from a USB stick on EFI hardware (e.g, an Apple device),
    edit the efi/boot/grub.cfg file.

Q: Why is the creation of a bootable USB stick so complicated now? With
   Kubrick 2.x, it was much easier!
A: Yes, the new method is slightly more complicated initially, but it's much
   more versatile. In the long run, it'll make things easier for all of us:
   - It supports devices which weren't previously supported.
   - Updates to newer Kubrick versions will normally only require to replace
     a single file on the USB stick, instead of reformatting it.
   - And last but not least: it reduces the overall software complexity.

Q: Will Kubrick ever allow to debrick Kindle 1, 2, DX, Paperwhite?
A: No, because these devices cannot be debricked over USB. Sorry.

Q: Why "Kubrick"?
A: Just because :-) I initially code-named this project KindleDebrick, then
   KDebrick, and then ended up with Kubrick.

-- ixtab

