package com.mobileread.ixtab.kubrick;

import java.nio.charset.Charset;

import com.googlecode.lanterna.TerminalFacade;
import com.googlecode.lanterna.gui.GUIScreen;
import com.googlecode.lanterna.terminal.Terminal;
import com.mobileread.ixtab.kubrick.developer.DeveloperMode;
import com.mobileread.ixtab.kubrick.lanterna.KubrickTheme;
import com.mobileread.ixtab.kubrick.model.Model;
import com.mobileread.ixtab.kubrick.view.MainWindow;

public class Kubrick {

	public static void main(String[] args) throws Exception {

		Terminal terminal = null;

		if (DeveloperMode.TEXTMODE) {
			terminal = TerminalFacade.createTerminal(Charset.forName("UTF8"));
		} else {
			terminal = DeveloperMode.getSwingTerminal();
		}
		System.out.println(Metadata.getProductWithVersion() + " started (text="
				+ DeveloperMode.TEXTMODE + ", debug=" + DeveloperMode.DEBUGMODE + ")");
		/*, display size "
				+ terminal.getTerminalSize().getColumns() + "x"
				+ terminal.getTerminalSize().getRows());*/
		GUIScreen ui = TerminalFacade.createGUIScreen(terminal);
		if (ui == null) {
			System.err.println("Couldn't allocate a terminal!");
			return;
		}
		KubrickTheme theme = new KubrickTheme();
		ui.setTheme(theme);
		// ui.setTitle("Kubrick "+VERSION);
		ui.setBackgroundRenderer(theme);
		ui.getScreen().startScreen();

		Model model = new Model(ui);
		MainWindow myWindow = new MainWindow(model);
		ui.showWindow(myWindow, GUIScreen.Position.CENTER);

		ui.getScreen().stopScreen();
	}

}
