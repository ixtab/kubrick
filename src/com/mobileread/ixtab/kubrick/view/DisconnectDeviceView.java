package com.mobileread.ixtab.kubrick.view;

import com.googlecode.lanterna.gui.Action;
import com.googlecode.lanterna.gui.component.Label;
import com.googlecode.lanterna.gui.component.Panel;
import com.googlecode.lanterna.terminal.Terminal.Color;
import com.mobileread.ixtab.kubrick.controller.Controller;
import com.mobileread.ixtab.kubrick.controller.Controller.ButtonType;
import com.mobileread.ixtab.kubrick.lanterna.MaxVerticalLayout;
import com.mobileread.ixtab.kubrick.model.Model;

public class DisconnectDeviceView extends View {

	protected DisconnectDeviceView(Model model, Controller controller) {
		super(model, controller);
	}

	@Override
	public void display(Panel panel) {
		controller.setTitle("Disconnect your Kindle", Color.BLACK);
		panel.setLayoutManager(new MaxVerticalLayout(true, true));

		StringBuilder msg = new StringBuilder();
		// MAX LENGTH: #####################################################################
		msg.append("Please make sure that your Kindle is DISCONNECTED from the computer.\n");
		msg.append("\nThat is, UNPLUG the cable from the computer to the Kindle.\n");
		msg.append("\nPress NEXT when you ensured that your Kindle is DISCONNECTED,\n");
		msg.append("and you're ready to proceed.");
		panel.addComponent(new Label(msg.toString()));

		controller.setButtonAction(ButtonType.NEXT, new Action() {
			@Override
			public void doAction() {
				model.setDisconnectedUsbNetworks();
				controller.setView(new UsbDownloaderView(model, controller));
			}
		});
	}

}
