package com.mobileread.ixtab.kubrick.view;

import com.googlecode.lanterna.gui.component.Label;
import com.googlecode.lanterna.gui.component.Panel;
import com.googlecode.lanterna.terminal.Terminal.Color;
import com.mobileread.ixtab.kubrick.controller.Controller;
import com.mobileread.ixtab.kubrick.lanterna.MaxVerticalLayout;
import com.mobileread.ixtab.kubrick.model.Model;

public class OtherActivitiesPerformView extends WaitingView {

	private final boolean doYKNR;
	private final boolean doJailbreak;
	private final boolean doBlastVarLocal;
	private final boolean doBlastMntUs;
	private final boolean doInstallTTS;
	
	protected OtherActivitiesPerformView(Model model, Controller controller, boolean yknr, boolean jailbreak, boolean blastVarLocal, boolean blastMntUs, boolean installTTS) {
		super(model, controller, false);
		this.doYKNR = blastVarLocal ? false : yknr;
		this.doJailbreak = jailbreak;
		this.doBlastVarLocal = blastVarLocal;
		this.doBlastMntUs = blastMntUs;
		this.doInstallTTS = installTTS;
	}
	
	@Override
	public void display(Panel panel) {
		controller.setTitle("Performing final actions", Color.BLACK);
		panel.setLayoutManager(new MaxVerticalLayout(true, true));

		StringBuilder msg = new StringBuilder();
		// MAX LENGTH: #####################################################################
		msg.append("We are now performing some final actions.\n");
		if (doInstallTTS) {
			msg.append("\nYou chose to install the TTS files, so this may take up to 15 minutes.\n");
			msg.append("Please be patient! :)\n");
		}
		msg.append("\nPress NEXT when the procedure has completed.\n\n ");
		
		panel.addComponent(new Label(msg.toString()));
		panel.addComponent(statusLabel);
		
		super.display(panel);
	}

	@Override
	protected Waiter getWaiter() {
		return new Waiter() {

			private volatile Worker worker;
			@Override
			protected int getIntervalMs() {
				return 1000;
			}

			@Override
			protected String getWaitingText() {
				return "Performing actions";
			}

			@Override
			protected String getReadyText() {
				return "Actions complete";
			}

			@Override
			protected boolean performWait() {
				if (worker == null) {
					worker = new Worker();
					new Thread(worker).start();
					return false;
				}
				if (worker.failed) {
					throw new RuntimeException("Actions failed");
				}
				return worker.finished;
			}
		};
	}

	@Override
	protected View getNextView() {
		return new RebootingView(model, controller);
	}
	
	private class Worker implements Runnable {
		public volatile boolean finished = false;
		public volatile boolean failed = false;
		

		@Override
		public void run() {
			try {
				if (doYKNR) {
					model.fixYourKindleNeedsRepair();
				}
				if (doJailbreak) {
					model.installJailbreak();
				}
				if (doBlastVarLocal) {
					model.blastVarLocal();
				}
				if (doBlastMntUs) {
					model.blastMntUs();
				}
				if (doInstallTTS) {
					model.reinstallTTSFiles();
				}
			} catch (Throwable t) {
				failed = true;
			}
			finished = true;
		}
	}
}
