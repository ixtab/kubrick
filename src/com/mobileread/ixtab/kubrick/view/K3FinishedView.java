package com.mobileread.ixtab.kubrick.view;

import com.googlecode.lanterna.gui.Action;
import com.googlecode.lanterna.gui.component.Label;
import com.googlecode.lanterna.gui.component.Panel;
import com.googlecode.lanterna.terminal.Terminal.Color;
import com.mobileread.ixtab.kubrick.controller.Controller;
import com.mobileread.ixtab.kubrick.controller.Controller.ButtonType;
import com.mobileread.ixtab.kubrick.lanterna.MaxVerticalLayout;
import com.mobileread.ixtab.kubrick.model.Model;

public class K3FinishedView extends View {

	protected K3FinishedView(Model model, Controller controller) {
		super(model, controller);
	}

	@Override
	public void display(Panel panel) {
		controller.setTitle("Reboot your Kindle", Color.BLACK);
		panel.setLayoutManager(new MaxVerticalLayout(true, true));
		
		StringBuilder msg = new StringBuilder();
		// MAX LENGTH: #####################################################################
		msg.append("Your Kindle has been flashed successfully.\n");
		msg.append("Please reboot the device now:\n\n");
		msg.append("Slide and hold the Power slider for AT LEAST 30 seconds.\n\n");
		msg.append("After a short while (up to a minute or so), your device should boot.");
		msg.append("\n\nPress NEXT when you're done.\n\n ");
		
		panel.addComponent(new Label(msg.toString()));
		
		controller.setButtonAction(ButtonType.NEXT, new Action() {
			@Override
			public void doAction() {
				controller.setView(new FinishedView(model, controller));
			}
		});
		
	}

}
