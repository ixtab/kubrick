package com.mobileread.ixtab.kubrick.view;

import com.googlecode.lanterna.gui.Action;
import com.googlecode.lanterna.gui.component.CheckBoxList;
import com.googlecode.lanterna.gui.component.Label;
import com.googlecode.lanterna.gui.component.Panel;
import com.googlecode.lanterna.terminal.Terminal.Color;
import com.mobileread.ixtab.kubrick.controller.Controller;
import com.mobileread.ixtab.kubrick.controller.Controller.ButtonType;
import com.mobileread.ixtab.kubrick.lanterna.MaxVerticalLayout;
import com.mobileread.ixtab.kubrick.model.Model;

public class FirmwareFlashConfirmView extends View {

	protected FirmwareFlashConfirmView(Model model, Controller controller) {
		super(model, controller);
	}
	
	private CheckBoxList choices = new CheckBoxList();
	
	@Override
	public void display(Panel panel) {
		
		controller.setTitle("Confirm overwriting of the main partition", Color.BLACK);
		panel.setLayoutManager(new MaxVerticalLayout(true, true));
		
		StringBuilder msg = new StringBuilder();
		// MAX LENGTH: #####################################################################
		msg.append("The next step is (normally) to flash the main partition of the device.\n ");
		panel.addComponent(new Label(msg.toString()));
		
		String option = "Yes, flash the main partition";
		choices.addItem(option);
		choices.setChecked(option, true);
		panel.addComponent(choices);
		
		msg = new StringBuilder();
		
		// MAX LENGTH: #####################################################################
		msg.append("\nYou usually HAVE to perform this action if your Kindle is BRICKED.\n\n");
		
		msg.append("However, in certain cases, you may skip this step. Such cases are:\n");
		msg.append(" - You only want to install the jailbreak on a working device.\n");
		msg.append(" - You only want to install the TTS files on a working Kindle Touch.\n");
		msg.append(" - You are an expert and you know why you want to skip it :-)\n");
		
		
		msg.append("\n\nPress NEXT when you're ready to proceed.");
		panel.addComponent(new Label(msg.toString()));
		
		controller.setButtonAction(ButtonType.NEXT, new GotoNextViewAction());

	}
	
	private class GotoNextViewAction implements Action {

		@Override
		public void doAction() {
			boolean flash = choices.isChecked(0);
			
			if (flash) {
				controller.setView(new FirmwareFlashPerformView(model, controller));
			} else {
				controller.setView(new OtherActivitiesSelectView(model, controller));
			}
		}
		
	}
}
