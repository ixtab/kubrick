package com.mobileread.ixtab.kubrick.view;

import com.googlecode.lanterna.gui.component.Label;
import com.googlecode.lanterna.gui.component.Panel;
import com.googlecode.lanterna.terminal.Terminal.Color;
import com.mobileread.ixtab.kubrick.controller.Controller;
import com.mobileread.ixtab.kubrick.lanterna.MaxVerticalLayout;
import com.mobileread.ixtab.kubrick.model.Model;

public class K3WaitForDownloadView extends WaitingView {

	protected K3WaitForDownloadView(Model model, Controller controller) {
		super(model, controller, false);
	}
	
	@Override
	public void display(Panel panel) {
		controller.setTitle("Waiting for USB Downloader mode", Color.BLACK);
		panel.setLayoutManager(new MaxVerticalLayout(true, true));

		StringBuilder msg = new StringBuilder();
		// MAX LENGTH: #####################################################################
		msg.append("Your device should now be in USB downloader mode.\n\n");
		msg.append("\n\nPress NEXT once your device is detected.\n\n ");
		
		panel.addComponent(new Label(msg.toString()));
		panel.addComponent(statusLabel);
		
		super.display(panel);
	}

	@Override
	protected Waiter getWaiter() {
		return new Waiter() {

			private volatile Worker worker;
			@Override
			protected int getIntervalMs() {
				return 1000;
			}

			@Override
			protected String getWaitingText() {
				return "Waiting for device";
			}

			@Override
			protected String getReadyText() {
				return "Device ready";
			}

			@Override
			protected boolean performWait() {
				if (worker == null) {
					worker = new Worker();
					new Thread(worker).start();
					return false;
				}
				if (worker.failed) {
					throw new RuntimeException("Operation failed");
				}
				return worker.finished;
			}
		};
	}

	@Override
	protected View getNextView() {
		return new K3FlashKernelView(model, controller);
	}
	
	private class Worker implements Runnable {
		public volatile boolean finished = false;
		public volatile boolean failed = false;
		

		@Override
		public void run() {
			try {
				model.waitForK3USBDownloaderMode();
			} catch (Throwable t) {
				failed = true;
			}
			finished = true;
		}
	}
}
