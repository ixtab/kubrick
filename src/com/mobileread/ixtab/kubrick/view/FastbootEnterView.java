package com.mobileread.ixtab.kubrick.view;

import com.googlecode.lanterna.gui.component.Label;
import com.googlecode.lanterna.gui.component.Panel;
import com.googlecode.lanterna.terminal.Terminal.Color;
import com.mobileread.ixtab.kubrick.controller.Controller;
import com.mobileread.ixtab.kubrick.lanterna.MaxVerticalLayout;
import com.mobileread.ixtab.kubrick.model.Model;

public class FastbootEnterView extends WaitingView {

	protected FastbootEnterView(Model model, Controller controller) {
		super(model, controller, true);
	}
	
	@Override
	public void display(Panel panel) {
		controller.setTitle("Putting device into Fastboot mode", Color.BLACK);
		panel.setLayoutManager(new MaxVerticalLayout(true, true));

		StringBuilder msg = new StringBuilder();
		// MAX LENGTH: #####################################################################
		msg.append("Your device is now entering fastboot mode.\n");
		msg.append("In this mode, we will flash the diagnostics kernel and partition.\n");
		msg.append("\n\nPress NEXT once your Kindle is in Fastboot mode.\n\n ");
		
		panel.addComponent(new Label(msg.toString()));
		panel.addComponent(statusLabel);
		
		super.display(panel);
	}

	@Override
	protected Waiter getWaiter() {
		return new Waiter() {

			boolean initialized = false;
			@Override
			protected int getIntervalMs() {
				return 1000;
			}

			@Override
			protected String getWaitingText() {
				return "Waiting for device";
			}

			@Override
			protected String getReadyText() {
				return "Device ready";
			}

			@Override
			protected boolean performWait() {
				if (!initialized) {
					model.enterFastbootMode();
					initialized = true;
					return false;
				} else {
					return model.isFastbootDevicePresent();
				}
			}
		};
	}

	@Override
	protected View getNextView() {
		return new FastbootFlashView(model, controller);
	}
}
