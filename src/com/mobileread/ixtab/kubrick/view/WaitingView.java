package com.mobileread.ixtab.kubrick.view;

import com.googlecode.lanterna.gui.Action;
import com.googlecode.lanterna.gui.component.Panel;
import com.mobileread.ixtab.kubrick.controller.Controller;
import com.mobileread.ixtab.kubrick.controller.Controller.ButtonType;
import com.mobileread.ixtab.kubrick.lanterna.IndicatorLabel;
import com.mobileread.ixtab.kubrick.model.Model;

public abstract class WaitingView extends View {

	protected WaitingView(Model model, Controller controller, boolean isCancellable) {
		super(model, controller);
		this.isCancellable = isCancellable;
	}
	
	protected IndicatorLabel statusLabel = new IndicatorLabel();
	protected abstract Waiter getWaiter();
	
	private Waiter waiter = getWaiter();
	private final boolean isCancellable;

	protected abstract View getNextView();
	
	@Override
	public void display(Panel panel) {
		if (isCancellable) {
			controller.setButtonAction(ButtonType.CANCEL, new Action() {
				
				@Override
				public void doAction() {
					waiter.cancel();
					controller.setView(new WelcomeView(model, controller));
				}
			});
		} else {
			controller.setButtonAction(ButtonType.CANCEL, null);
		}
		new Thread(waiter).start();
	}

	
	public abstract class Waiter implements Runnable {

		protected abstract int getIntervalMs();
		protected abstract String getWaitingText();
		protected abstract String getReadyText();
		protected abstract boolean performWait() throws Throwable;
		
		private volatile boolean canceled = false;
		
		@Override
		public void run() {
			String waitText = getWaitingText();
			int waitTextLength = waitText.length() + 3;
			int dots = 0;
			boolean firstIteration = true;
			boolean failed = false;
			while(true) {
				if (canceled) {
					return;
				}
				StringBuilder sb = new StringBuilder();
				if (isWaitingTextDynamic()) {
					sb.append(getWaitingText());
				} else {
					sb.append(waitText);
					for (int i=0; i < dots; ++i) {
						sb.append(".");
					}
					while (sb.length() < waitTextLength) {
						sb.append(" ");
					}
				}
				statusLabel.setText(sb.toString());
				if (firstIteration) {
					// really just a psychological thing:
					// don't do anything on the first
					// iteration
					firstIteration = false;
				} else {
					try {
						boolean done = performWait();
						if (done) {
							break;
						}
					} catch (Throwable t) {
						failed = true;
						break;
					}
				}
				try {
					Thread.sleep(getIntervalMs());
				} catch (InterruptedException e) {
				}
				dots = (dots + 1) % 4;
			}
			if (failed) {
				statusLabel.setText("FAILED");
				controller.setButtonAction(ButtonType.CANCEL, null);
				controller.setButtonAction(ButtonType.NEXT, null);
				return;
			}
			statusLabel.setCompleted(true);
			statusLabel.setText(getReadyText());
			controller.setButtonAction(ButtonType.NEXT, new Action() {
				@Override
				public void doAction() {
					controller.setView(getNextView());
				}

			});

		}
		
		protected boolean isWaitingTextDynamic() {
			return false;
		}
		
		public void cancel() {
			canceled = true;
		}
	}
}
