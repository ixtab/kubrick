package com.mobileread.ixtab.kubrick.view;

import java.io.BufferedReader;
import java.io.InputStreamReader;

import com.googlecode.lanterna.gui.component.Label;
import com.googlecode.lanterna.gui.component.Panel;
import com.googlecode.lanterna.terminal.Terminal.Color;
import com.mobileread.ixtab.kubrick.controller.Controller;
import com.mobileread.ixtab.kubrick.lanterna.MaxVerticalLayout;
import com.mobileread.ixtab.kubrick.model.Model;

public class K3FlashMainView extends WaitingView {

	protected K3FlashMainView(Model model, Controller controller) {
		super(model, controller, false);
	}
	
	@Override
	public void display(Panel panel) {
		controller.setTitle("Flashing Main Partition", Color.BLACK);
		panel.setLayoutManager(new MaxVerticalLayout(true, true));

		StringBuilder msg = new StringBuilder();
		// MAX LENGTH: #####################################################################
		msg.append("We are now flashing the main partition.\n");
		msg.append("\nThis will take a long time (usually between 1 and 3 hours).\n");
		msg.append("\n\nPress NEXT when the procedure has completed.\n\n ");
		
		panel.addComponent(new Label(msg.toString()));
		panel.addComponent(statusLabel);
		
		super.display(panel);
	}

	@Override
	protected Waiter getWaiter() {
		
		return new Waiter() {

			private volatile Worker worker;
			@Override
			protected int getIntervalMs() {
				return 1000;
			}

			@Override
			protected boolean isWaitingTextDynamic() {
				return true;
			}

			@Override
			protected String getWaitingText() {
				if (worker == null) {
					return "Flashing main partition... ";
				} else {
					String r = "Flashing main partition... " + worker.getPercentage()+"%";
					return r;
				}
			}

			@Override
			protected String getReadyText() {
				return "Flashing done";
			}

			@Override
			protected boolean performWait() {
				if (worker == null) {
					worker = new Worker();
					new Thread(worker).start();
					return false;
				}
				if (worker.failed) {
					throw new RuntimeException("Operation failed");
				}
				return worker.finished;
			}
		};
	}

	@Override
	protected View getNextView() {
		return new K3FinishedView(model, controller);
	}
	
	private class Worker implements Runnable {
		public volatile boolean finished = false;
		public volatile boolean failed = false;
		
		private volatile int percent = 0;
		
		public int getPercentage() {
			return percent;
		}


		@Override
		public void run() {
			try {
				Process p = model.flashK3Main();
				ProcessWaiter pw = new ProcessWaiter(p);
				pw.start();
				BufferedReader output = new BufferedReader(new InputStreamReader(p.getInputStream()));
				while (!pw.isDone()) {
					for (String line = output.readLine(); line != null; line = output.readLine()) {
						if (line.endsWith("%")) {
							String pString = line.substring(line.lastIndexOf(" "));
							pString = pString.replace(" ", "");
							pString = pString.replace("%", "");
							try {
								float pFloat = Float.parseFloat(pString);
								percent = Math.round(pFloat);
							} catch (Throwable t) {
								
							}
						}
					}
				}
			} catch (Throwable t) {
				failed = true;
			}
			finished = true;
		}
	}
	
	private static class ProcessWaiter extends Thread {

		private final Process process;
		private volatile boolean done = false;
		
		public ProcessWaiter(Process process) {
			super();
			this.process = process;
		}

		@Override
		public void run() {
			while (!done) {
				try {
					process.waitFor();
					done = true;
				} catch (InterruptedException e) {}
			}
		}
		
		public boolean isDone() {
			return done;
		}
	}
}
