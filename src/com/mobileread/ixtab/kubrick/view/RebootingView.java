package com.mobileread.ixtab.kubrick.view;

import com.googlecode.lanterna.gui.component.Label;
import com.googlecode.lanterna.gui.component.Panel;
import com.googlecode.lanterna.terminal.Terminal.Color;
import com.mobileread.ixtab.kubrick.controller.Controller;
import com.mobileread.ixtab.kubrick.lanterna.MaxVerticalLayout;
import com.mobileread.ixtab.kubrick.model.Model;

public class RebootingView extends WaitingView {

	public RebootingView(Model model, Controller controller) {
		super(model, controller, true);
	}

	@Override
	public void display(Panel panel) {
		controller.setTitle("Rebooting your Kindle", Color.BLACK);
		panel.setLayoutManager(new MaxVerticalLayout(true, true));

		StringBuilder msg = new StringBuilder();
		// MAX LENGTH:
		// #####################################################################
		msg.append("Your Kindle should now be rebooting into its normal operation mode.\n\n");
		msg.append("If the procedure has completed without error, then:\n\n");
		msg.append("CONGRATULATIONS! YOUR DEVICE IS NOW FUNCTIONAL AGAIN.\n");
		msg.append("-----------------------------------------------------\n\n");
		msg.append("\nSelect NEXT when you're ready to proceed.\n ");

		panel.addComponent(new Label(msg.toString()));
		panel.addComponent(statusLabel);

		super.display(panel);
	}

	@Override
	protected Waiter getWaiter() {
		return new Waiter() {

			volatile boolean initialized = false;

			@Override
			protected int getIntervalMs() {
				return 1000;
			}

			@Override
			protected String getWaitingText() {
				return "Rebooting device";
			}

			@Override
			protected String getReadyText() {
				return "Debricking complete!";
			}

			@Override
			protected boolean performWait() {
				if (!initialized) {
					model.rebootToMain();
					initialized = true;
					return false;
				} else {
					return true;
				}
			}
		};
	}

	@Override
	protected View getNextView() {
		return new FinishedView(model, controller);
	}

}
