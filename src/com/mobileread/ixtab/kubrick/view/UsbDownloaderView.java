package com.mobileread.ixtab.kubrick.view;

import com.googlecode.lanterna.gui.component.Label;
import com.googlecode.lanterna.gui.component.Panel;
import com.googlecode.lanterna.terminal.Terminal.Color;
import com.mobileread.ixtab.kubrick.controller.Controller;
import com.mobileread.ixtab.kubrick.lanterna.MaxVerticalLayout;
import com.mobileread.ixtab.kubrick.model.Model;

public class UsbDownloaderView extends WaitingView {

	protected UsbDownloaderView(Model model, Controller controller) {
		super(model, controller, true);
	}
	
	@Override
	public void display(Panel panel) {
		controller.setTitle("Put your Kindle into USB downloader mode", Color.BLACK);
		panel.setLayoutManager(new MaxVerticalLayout(true, true));

		StringBuilder msg = new StringBuilder();
		String key = model.getConfiguration().magicKey;
		// MAX LENGTH: #####################################################################
		msg.append("You will now put your device into USB downloader mode.\n\n");
		msg.append("1. Connect the Kindle to your Computer.\n");
		msg.append("2. Press and hold the Kindle's power button, and keep it pressed.\n");
		msg.append("3. When the LED turns off, keep pressing the power button, and in\n");
		msg.append("   addition, press the ");
		msg.append(key);
		msg.append(".\n");
		msg.append("4. Release the power button.\n");
		msg.append("5. Release the ");
		msg.append(key);
		msg.append(".\n\nPress NEXT once your Kindle is in USB downloader mode.\n\n ");
		
		panel.addComponent(new Label(msg.toString()));
		panel.addComponent(statusLabel);
		
		super.display(panel);
	}

	@Override
	protected Waiter getWaiter() {
		return new Waiter() {

			@Override
			protected int getIntervalMs() {
				return 1000;
			}

			@Override
			protected String getWaitingText() {
				return "Waiting for device";
			}

			@Override
			protected String getReadyText() {
				return "Device ready";
			}

			@Override
			protected boolean performWait() {
				return model.isUsbDownloaderDevicePresent();
			}
		};
	}

	@Override
	protected View getNextView() {
		return new FastbootEnterView(model, controller);
	}
}
