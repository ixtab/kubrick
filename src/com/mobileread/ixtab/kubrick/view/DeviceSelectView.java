package com.mobileread.ixtab.kubrick.view;

import java.util.ArrayList;
import java.util.List;

import com.googlecode.lanterna.gui.Action;
import com.googlecode.lanterna.gui.component.Label;
import com.googlecode.lanterna.gui.component.Panel;
import com.googlecode.lanterna.terminal.Terminal.Color;
import com.mobileread.ixtab.kubrick.controller.Controller;
import com.mobileread.ixtab.kubrick.controller.Controller.ButtonType;
import com.mobileread.ixtab.kubrick.lanterna.MaxVerticalLayout;
import com.mobileread.ixtab.kubrick.model.DebrickConfiguration;
import com.mobileread.ixtab.kubrick.model.Model;

public class DeviceSelectView extends View {

	public DeviceSelectView(Model model, Controller controller) {
		super(model, controller);
	}
	
	@Override
	public void display(Panel panel) {
		controller.setTitle("Select Kindle Device", Color.BLACK);
		panel.setLayoutManager(new MaxVerticalLayout(true, true));
		
		Object[] options = createConfigurationOptions();
		
		StringBuilder msg = new StringBuilder();
		panel.addComponent(new Label("Please select your device from the list below.\n \n"));
		panel.addComponent(new Label("    Device                   Firmware version      Overwrite firmware\n"));
		panel.addComponent(new ConfigurationsList(options));
		
		// MAX LENGTH: #####################################################################
		msg.append("\n* For devices where overwriting the firmware is OPTIONAL, Kubrick will\n");
		msg.append("  later give you the choice to either overwrite the firmware, or keep\n");
		msg.append("  the currently installed one.\n");
		msg.append("* MANDATORY means that Kubrick will always overwrite the firmware.\n");
		panel.addComponent(new Label(msg.toString()));
		
		msg = new StringBuilder();
		if (options.length > 1) {
			msg.append("\nPress NEXT when you have selected a device and are ready to proceed.");
		} else {
			msg.append("\nPress NEXT when you are ready to proceed.");
		}
		panel.addComponent(new Label(msg.toString()));
		
	}

	public void setConfiguration(DebrickConfiguration configuration) {
		model.setConfiguration(configuration);
		if (configuration.usesK3Flasher()) {
			controller.setButtonAction(ButtonType.NEXT, new Action() {
				
				@Override
				public void doAction() {
					controller.setView(new K3PrepareView(model, controller));
				}
			});
		} else {
			controller.setButtonAction(ButtonType.NEXT, new Action() {
				
				@Override
				public void doAction() {
					if (model.isDisconnectedUsbNetworksSet()) {
						controller.setView(new UsbDownloaderView(model, controller));
					} else {
						controller.setView(new DisconnectDeviceView(model, controller));
					}
				}
			});
		}
	}	
	
	private Object[] createConfigurationOptions() {
		List<Object> list = new ArrayList<Object>();
		for (DebrickConfiguration conf: model.getAvailableConfigurations()) {
			if (conf.isAvailable()) {
				list.add(conf);
			}
		}
		return list.toArray();
	}
	
	@Override
	protected void setOption(int index, Object item) {
		setConfiguration((DebrickConfiguration) item);
	}
	
	private class ConfigurationsList extends OptionsList {
		public ConfigurationsList(Object[] options) {
			super(options);
			if (options.length == 1) {
				setCheckedItemIndex(0);
    			DeviceSelectView.this.setConfiguration((DebrickConfiguration) this.getCheckedItem());
			}
		}
	}

}
