package com.mobileread.ixtab.kubrick.view;

import com.googlecode.lanterna.gui.Action;
import com.googlecode.lanterna.gui.Border;
import com.googlecode.lanterna.gui.Window;
import com.googlecode.lanterna.gui.component.Button;
import com.googlecode.lanterna.gui.component.Label;
import com.googlecode.lanterna.gui.component.Panel;
import com.googlecode.lanterna.gui.component.Panel.Orientation;
import com.googlecode.lanterna.gui.layout.BorderLayout;
import com.googlecode.lanterna.terminal.Terminal;
import com.googlecode.lanterna.terminal.Terminal.Color;
import com.googlecode.lanterna.terminal.TerminalSize;
import com.mobileread.ixtab.kubrick.controller.Controller;
import com.mobileread.ixtab.kubrick.lanterna.MaxBorderLayout;
import com.mobileread.ixtab.kubrick.lanterna.MaxVerticalLayout;
import com.mobileread.ixtab.kubrick.model.Model;
import com.mobileread.ixtab.kubrick.developer.DeveloperMode;

public class MainWindow extends Window implements Controller {

	private final MutableActionButton nextButton = new MutableActionButton(
			"Next");

	private final MutableActionButton cancelButton = new MutableActionButton(
			"Cancel");

	private Action nextButtonAction = null;

	private final Label title = new Label("", 80, Terminal.Color.BLACK, true);
	private final Panel mainPanel = new Panel();

	private final Model model;

	private final Action defaultCancelAction = new Action() {

		@Override
		public void doAction() {
			MainWindow.this.setView(new WelcomeView(model, MainWindow.this));
		}

	};

	public MainWindow(Model model) {
		super("");
		this.model = model;
		setWindowSizeOverride(new TerminalSize(80, 25));

		Panel titlePanel = new Panel(new Border.Bevel(true),
				Orientation.VERTICAL);
		titlePanel.addComponent(title, BorderLayout.TOP);
		this.addComponent(titlePanel, BorderLayout.TOP);

		mainPanel.setBorder(new Border.Standard());
		mainPanel.setLayoutManager(new MaxVerticalLayout(true, true));
		this.addComponent(mainPanel, BorderLayout.CENTER);

		Panel buttonsPanel = new Panel();
		buttonsPanel.setLayoutManager(new MaxBorderLayout(false, true));
		buttonsPanel.addComponent(nextButton, BorderLayout.LEFT);
		buttonsPanel.addComponent(cancelButton, BorderLayout.RIGHT);

		this.addComponent(buttonsPanel, BorderLayout.BOTTOM);

		View view =null;
		if (DeveloperMode.isEnabled()) {
			// developer mode may (but doesn't have to) override initial view
			view = DeveloperMode.getInitialView(model, this);
		}
		if (view == null) {
			// default view
			view = new WelcomeView(model, this);
		}
		setView(view);
	}

	protected void onNextButton() {
		if (nextButtonAction == null) {
			close();
			return;
		}
		nextButtonAction.doAction();
	}

	protected void onCancelButton() {
		setView(new WelcomeView(model, this));
	}

	@Override
	public void setView(View view) {
		// currentView = view;
		setButtonAction(ButtonType.NEXT, null);
		setButtonAction(ButtonType.CANCEL, defaultCancelAction);
		mainPanel.removeAllComponents();
		view.display(mainPanel);
	}

	@Override
	public void setTitle(String status, Color color) {
		// this.status.setStyle(Category.SHADOW);
		this.title.setText(status);
		if (color != null) {
			this.title.setTextColor(color);
		}
	}

	@Override
	public void setButtonAction(ButtonType buttonType, Action action) {
		MutableActionButton button = getButtonForType(buttonType);
		if (action == null) {
			button.setVisible(false);
		} else {
			button.setVisible(true);
			button.setAction(action);
		}
	}

	@Override
	public Action getButtonAction(ButtonType buttonType) {
		return getButtonForType(buttonType).action;
	}

	private MutableActionButton getButtonForType(ButtonType buttonType) {
		switch (buttonType) {
		case NEXT:
			return nextButton;
		case CANCEL:
			return cancelButton;
		default:
			return null;
		}
	}

	private static class MutableAction implements Action {

		private Action delegate = null;

		@Override
		public void doAction() {
			if (delegate != null) {
				delegate.doAction();
			}
		}

		public void setDelegate(Action delegateAction) {
			this.delegate = delegateAction;
		}

	}

	private static class MutableActionButton extends Button {

		private final MutableAction action;

		public MutableActionButton(String name) {
			this(name, new MutableAction());
		}

		public MutableActionButton(String name, MutableAction mutableAction) {
			super(name, mutableAction);
			this.action = mutableAction;
		}

		public void setAction(Action delegateAction) {
			action.setDelegate(delegateAction);
		}

	}

	@Override
	public void exit() {
		close();
	}
}
