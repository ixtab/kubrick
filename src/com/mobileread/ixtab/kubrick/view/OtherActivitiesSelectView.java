package com.mobileread.ixtab.kubrick.view;

import com.googlecode.lanterna.gui.Action;
import com.googlecode.lanterna.gui.component.CheckBoxList;
import com.googlecode.lanterna.gui.component.Label;
import com.googlecode.lanterna.gui.component.Panel;
import com.googlecode.lanterna.terminal.Terminal.Color;
import com.mobileread.ixtab.kubrick.controller.Controller;
import com.mobileread.ixtab.kubrick.controller.Controller.ButtonType;
import com.mobileread.ixtab.kubrick.lanterna.MaxVerticalLayout;
import com.mobileread.ixtab.kubrick.model.Model;

public class OtherActivitiesSelectView extends View {

	public OtherActivitiesSelectView(Model model, Controller controller) {
		super(model, controller);
	}

	private static String[] options = new String[] {
			"Install Device Jailbreak", "Wipe Internal Storage", };

	private CheckBoxList choices = new CheckBoxList();
	private int reformatExternalChoiceIndex = -1;
	private int installTtsChoiceIndex = -1;

	@Override
	public void display(Panel panel) {

		boolean canBlastExternal = model.canBlastMntUs();
		boolean hasTts = model.getConfiguration().hasTTS();
		boolean canInstallTts = hasTts ? (canBlastExternal && model
				.canTTSFilesBeInstalled()) : false;

		controller
				.setTitle("Select additional actions to perform", Color.BLACK);
		panel.setLayoutManager(new MaxVerticalLayout(true, true));

		StringBuilder msg = new StringBuilder();
		// MAX LENGTH:
		// #####################################################################
		msg.append("Simply press NEXT to use the default settings if you're unsure.\n \n");
		panel.addComponent(new Label(msg.toString()));

		for (String option : options) {
			choices.addItem(option);
			// quick and dirty
			if (option.toLowerCase().contains("jailbreak")) {
				choices.setChecked(option, true);
			}
		}
		if (canBlastExternal) {
			reformatExternalChoiceIndex = choices.getSize();
			choices.addItem("Reformat External (USB) Storage");
		}
		if (canInstallTts) {
			installTtsChoiceIndex = choices.getSize();
			choices.addItem("Reinstall Text-To-Speech Files");
		}
		panel.addComponent(choices);

		msg = new StringBuilder();

		// MAX LENGTH:
		// #####################################################################
		msg.append("\n* Installing the device jailbreak is recommended.\n");
		msg.append("* Wiping the INTERNAL storage will RESET all your SETTINGS.\n");
		if (canBlastExternal) {
			msg.append("* Reformatting the EXTERNAL storage will DELETE all your CONTENT.\n");
			if (hasTts) {
				if (canInstallTts) {
					msg.append("* Reinstall the Text-to-Speech files if you have deleted them, want to\n");
					msg.append("  revert to the english voices, or are reformatting the USB storage.\n");
				} else {
					msg.append("\nNOTE: The option to reinstall TTS files is not available because an\n");
					msg.append("      error occured (most probably there is not enough disk space).\n");
				}
			}
		} else {
			msg.append("\nNOTE: Some options are not available because your device seems to be\n");
			msg.append("      in a really fucked up state.\n");
		}

		panel.addComponent(new Label(msg.toString()));
		//panel.addComponent(new Label("\nPress NEXT when you're ready to proceed."));

		controller.setButtonAction(ButtonType.NEXT, new GotoNextViewAction());

	}

	private class GotoNextViewAction implements Action {

		@Override
		public void doAction() {
			boolean fixYKNR = true;
			boolean jailbreak = choices.isChecked(0);
			boolean blastVarLocal = choices.isChecked(1);
			boolean blastMntUs = reformatExternalChoiceIndex == -1 ? false
					: Boolean.TRUE.equals(choices
							.isChecked(reformatExternalChoiceIndex));
			boolean reinstallTTS = installTtsChoiceIndex == -1 ? false
					: Boolean.TRUE.equals(choices
							.isChecked(installTtsChoiceIndex));

			if (fixYKNR || jailbreak || blastVarLocal || reinstallTTS) {
				controller.setView(new OtherActivitiesPerformView(model,
						controller, fixYKNR, jailbreak, blastVarLocal,
						blastMntUs, reinstallTTS));
			} else {
				controller.setView(new RebootingView(model, controller));
			}
		}

	}
}
