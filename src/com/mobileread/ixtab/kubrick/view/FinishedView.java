package com.mobileread.ixtab.kubrick.view;

import com.googlecode.lanterna.gui.Action;
import com.googlecode.lanterna.gui.component.Label;
import com.googlecode.lanterna.gui.component.Panel;
import com.googlecode.lanterna.terminal.Terminal.Color;
import com.mobileread.ixtab.kubrick.controller.Controller;
import com.mobileread.ixtab.kubrick.controller.Controller.ButtonType;
import com.mobileread.ixtab.kubrick.lanterna.MaxVerticalLayout;
import com.mobileread.ixtab.kubrick.model.Model;

public class FinishedView extends View {

	protected FinishedView(Model model, Controller controller) {
		super(model, controller);
	}

	private static String[] options = new String[] {
		"Shut down the debricking environment",
		"Start over, and debrick another Kindle",
	};
	
	@Override
	public void display(Panel panel) {
		controller.setButtonAction(ButtonType.CANCEL, null);
		
		controller.setTitle("What do you want to do now?", Color.BLACK);
		panel.setLayoutManager(new MaxVerticalLayout(true, true));
		
		StringBuilder msg = new StringBuilder();
		// MAX LENGTH: #####################################################################
		msg.append("Your Kindle should be debricked and working by now. What now?\n ");
		panel.addComponent(new Label(msg.toString()));
		
		OptionsList options = new OptionsList(FinishedView.options);
		panel.addComponent(options);
		
		msg = new StringBuilder();
		// MAX LENGTH: #####################################################################
		msg.append("\nOh... Just before you leave, I do have one final suggestion:\n");
		msg.append("If you think this program was helpful to you, then please consider\n");
		msg.append("doing something helpful in return. If you can, please donate to a\n");
		msg.append("literacy project which makes books available to children in Laos:\n\n");
		msg.append("                     www.laosliteracyproject.org\n\n");
		msg.append("Thank you!!!");
		panel.addComponent(new Label(msg.toString()));
		
	}
	
	@Override
	protected void setOption(int index, Object item) {
		if (index == 0) {
			// shutdown
			controller.setButtonAction(ButtonType.NEXT, new Action() {
				
				@Override
				public void doAction() {
					model.shutdown();
				}
			});
		} else if (index == 1) {
			controller.setButtonAction(ButtonType.NEXT, new Action(){

				@Override
				public void doAction() {
					controller.setView(new DeviceSelectView(model, controller));
				}});
		}
	}

}
