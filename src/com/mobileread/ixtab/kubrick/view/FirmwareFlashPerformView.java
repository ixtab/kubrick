package com.mobileread.ixtab.kubrick.view;

import com.googlecode.lanterna.gui.component.Label;
import com.googlecode.lanterna.gui.component.Panel;
import com.googlecode.lanterna.terminal.Terminal.Color;
import com.mobileread.ixtab.kubrick.controller.Controller;
import com.mobileread.ixtab.kubrick.lanterna.MaxVerticalLayout;
import com.mobileread.ixtab.kubrick.model.Model;

public class FirmwareFlashPerformView extends WaitingView {

	protected FirmwareFlashPerformView(Model model, Controller controller) {
		super(model, controller, false);
	}
	
	@Override
	public void display(Panel panel) {
		controller.setTitle("Flashing main kernel and partition", Color.BLACK);
		panel.setLayoutManager(new MaxVerticalLayout(true, true));

		StringBuilder msg = new StringBuilder();
		// MAX LENGTH: #####################################################################
		msg.append("We are now flashing the main kernel and partition.\n");
		msg.append("\nThis will normally take between 3 and 10 minutes, so please...\n");
		msg.append("\n\n                             BE PATIENT!!!\n\n\n");
		msg.append("Press NEXT when the procedure has completed.\n\n ");
		
		panel.addComponent(new Label(msg.toString()));
		panel.addComponent(statusLabel);
		
		super.display(panel);
	}

	@Override
	protected Waiter getWaiter() {
		return new Waiter() {

			private volatile Flasher flasher;
			@Override
			protected int getIntervalMs() {
				return 1000;
			}

			@Override
			protected String getWaitingText() {
				return "Flashing";
			}

			@Override
			protected String getReadyText() {
				return "Flashing complete";
			}

			@Override
			protected boolean performWait() {
				if (flasher == null) {
					flasher = new Flasher();
					new Thread(flasher).start();
					return false;
				}
				if (flasher.failed) {
					throw new RuntimeException("flashing failed");
				}
				return flasher.finished;
			}
		};
	}

	@Override
	protected View getNextView() {
		return new OtherActivitiesSelectView(model, controller);
	}
	
	private class Flasher implements Runnable {
		public volatile boolean finished = false;
		public volatile boolean failed = false;
		

		@Override
		public void run() {
			try {
				model.flashMain();
			} catch (Throwable t) {
				failed = true;
			}
			finished = true;
		}
		
	}
}
