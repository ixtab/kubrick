package com.mobileread.ixtab.kubrick.view;

import com.googlecode.lanterna.gui.Action;
import com.googlecode.lanterna.gui.GUIScreen;
import com.googlecode.lanterna.gui.component.Label;
import com.googlecode.lanterna.gui.component.Panel;
import com.googlecode.lanterna.gui.dialog.DialogButtons;
import com.googlecode.lanterna.gui.dialog.DialogResult;
import com.googlecode.lanterna.gui.dialog.MessageBox;
import com.googlecode.lanterna.terminal.Terminal.Color;
import com.mobileread.ixtab.kubrick.controller.Controller;
import com.mobileread.ixtab.kubrick.controller.Controller.ButtonType;
import com.mobileread.ixtab.kubrick.lanterna.MaxVerticalLayout;
import com.mobileread.ixtab.kubrick.model.Model;

public class WelcomeView extends View {

	protected WelcomeView(Model model, Controller controller) {
		super(model, controller);
	}

	@Override
	public void display(Panel panel) {
		controller.setTitle("Welcome to Kubrick!", Color.BLACK);
		panel.setLayoutManager(new MaxVerticalLayout(true, true));
		
		StringBuilder msg = new StringBuilder();
		// MAX LENGTH: #####################################################################
		msg.append("Kubrick uses a simple text-based dialog wizard, which will (normally)\n");
		msg.append("quickly and easily recover your Kindle.\n\n");
		msg.append("IMPORTANT NOTES:\n");
		msg.append("  1. Make SURE that the Kindle battery is fully charged BEFORE trying\n");
		msg.append("     to debrick it. Leave it on a WALL CHARGER for a day if in doubt.\n");
		msg.append("  2. Kubrick can help in most circumstances, but not in ALL of them.\n");
		msg.append("     If your device is really SEVERELY bricked, you may still have to\n");
		msg.append("     manually debrick it.\n");
		msg.append("  3. Don't use the mouse. Instead, use the arrow keys, and the\n");
		msg.append("     ENTER key, to make your choices.\n");
		msg.append("\n");
		msg.append("Use the NEXT button below when you're ready. (i.e.: press ENTER ;-) )");
		panel.addComponent(new Label(msg.toString()));
		
		controller.setButtonAction(ButtonType.NEXT, new Action() {
			@Override
			public void doAction() {
				controller.setView(new DeviceSelectView(model, controller));
			}
		});
		
		controller.setButtonAction(ButtonType.CANCEL, new Action() {

			@Override
			public void doAction() {
				GUIScreen owner = model.owner;
				DialogResult result = MessageBox.showMessageBox(owner, "Exit Kubrick?", "\nAre you sure that you want to exit Kubrick and shut down the computer?", DialogButtons.YES_NO);
				if (result == DialogResult.YES) {
					model.shutdown();
				}
			}
			
		});

	}

}
