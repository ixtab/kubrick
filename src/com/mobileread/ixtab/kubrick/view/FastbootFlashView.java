package com.mobileread.ixtab.kubrick.view;

import com.googlecode.lanterna.gui.component.Label;
import com.googlecode.lanterna.gui.component.Panel;
import com.googlecode.lanterna.terminal.Terminal.Color;
import com.mobileread.ixtab.kubrick.controller.Controller;
import com.mobileread.ixtab.kubrick.lanterna.MaxVerticalLayout;
import com.mobileread.ixtab.kubrick.model.Model;

public class FastbootFlashView extends WaitingView {

	protected FastbootFlashView(Model model, Controller controller) {
		super(model, controller, false);
	}
	
	@Override
	public void display(Panel panel) {
		controller.setTitle("Flashing diags kernel and partition", Color.BLACK);
		panel.setLayoutManager(new MaxVerticalLayout(true, true));

		StringBuilder msg = new StringBuilder();
		// MAX LENGTH: #####################################################################
		msg.append("We are now flashing the diags system.\n");
		msg.append("This will usually take between 15 and 30 seconds.\n\n");
		msg.append("Your Kindle will also reboot into diagnostics mode, which will take\n");
		msg.append("another 20 seconds or so.\n");
		msg.append("\nPress NEXT when your Kindle displays the diagnostics mode.\n\n");
		msg.append("NOTE: Just in case... your Kindle WILL take around 20 seconds to\n");
		msg.append("      reboot into diagnostics mode, even after the status indicator\n");
		msg.append("      below turned green. Just give it that time!\n ");
		
		panel.addComponent(new Label(msg.toString()));
		panel.addComponent(statusLabel);
		
		super.display(panel);
	}

	@Override
	protected Waiter getWaiter() {
		return new Waiter() {

			private volatile Flasher flasher;
			@Override
			protected int getIntervalMs() {
				return 1000;
			}

			@Override
			protected String getWaitingText() {
				return "Flashing";
			}

			@Override
			protected String getReadyText() {
				return "Flashing complete";
			}

			@Override
			protected boolean performWait() {
				if (flasher == null) {
					flasher = new Flasher();
					new Thread(flasher).start();
					return false;
				}
				if (flasher.failed) {
					throw new RuntimeException("flashing failed");
				}
				return flasher.finished;
			}
		};
	}

	@Override
	protected View getNextView() {
		return new DiagsView(model, controller);
	}
	
	private class Flasher implements Runnable {
		public volatile boolean finished = false;
		public volatile boolean failed = false;
		

		@Override
		public void run() {
			try {
				model.flashDiags();
			} catch (Throwable t) {
				failed = true;
			}
			finished = true;
		}
		
	}
}
