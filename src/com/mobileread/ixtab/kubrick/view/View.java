package com.mobileread.ixtab.kubrick.view;

import com.googlecode.lanterna.gui.Interactable;
import com.googlecode.lanterna.gui.component.Panel;
import com.googlecode.lanterna.gui.component.RadioCheckBoxList;
import com.googlecode.lanterna.input.Key;
import com.mobileread.ixtab.kubrick.controller.Controller;
import com.mobileread.ixtab.kubrick.model.Model;

public abstract class View {

	protected final Model model;
	protected final Controller controller;
	public abstract void display(Panel panel);
	
	protected View(Model model, Controller controller) {
		this.model = model;
		this.controller = controller;
	}
	
	protected class OptionsList extends RadioCheckBoxList {
		public OptionsList(Object[] options) {
			super();
			for (Object option: options) {
				addItem(option);
			}
		}
		
		// the superclass uses a very weird way to handle selections... but well,
		// we're flexible. ;-)
	    @Override
	    protected Interactable.Result unhandledKeyboardEvent(Key key) {
	    	int indexBeforeEvent = getCheckedItemIndex();
	    	Interactable.Result result = super.unhandledKeyboardEvent(key);
	    	int indexAfterEvent = getCheckedItemIndex();
	    	if (indexBeforeEvent != indexAfterEvent) {
	    		if (indexAfterEvent >= 0) {
	    			setOption(this.getCheckedItemIndex(), this.getCheckedItem());
	    		}
	    	}
	    	return result;
	    }
	}
	
	protected void setOption(int index, Object item) {
		
	}
}
