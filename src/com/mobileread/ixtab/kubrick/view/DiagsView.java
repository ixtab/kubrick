package com.mobileread.ixtab.kubrick.view;

import com.googlecode.lanterna.gui.component.Label;
import com.googlecode.lanterna.gui.component.Panel;
import com.googlecode.lanterna.terminal.Terminal.Color;
import com.mobileread.ixtab.kubrick.controller.Controller;
import com.mobileread.ixtab.kubrick.lanterna.MaxVerticalLayout;
import com.mobileread.ixtab.kubrick.model.Model;

public class DiagsView extends WaitingView {

	protected DiagsView(Model model, Controller controller) {
		super(model, controller, true);
	}
	
	@Override
	public void display(Panel panel) {
		controller.setTitle("Diagnostics mode", Color.BLACK);
		panel.setLayoutManager(new MaxVerticalLayout(true, true));

		StringBuilder msg = new StringBuilder();
		// MAX LENGTH: #####################################################################
		msg.append("The debricking procedure is well on its way, but your interaction is\n");
		msg.append("required now: You have to select the following items on the Kindle,\n");
		msg.append("in the order outlined below. On the K4, use the five-way controller;\n");
		msg.append("on the Kindle Touch, use the Touch screen. Select the following items:\n");
		msg.append("1. \"N) Misc individual diagnostics\"\n");
		msg.append("2. \"U) Utilities\"\n");
		msg.append("3. \"Z) Enable USBNet\"\n");
		msg.append("4. \"X) Exit\"\n");
		msg.append("\nIt may take a while (around 30 seconds or so) before Kubrick sees\n");
		msg.append("the new state, so please be patient. Press NEXT when ready.\n ");
		
		panel.addComponent(new Label(msg.toString()));
		panel.addComponent(statusLabel);
		
		super.display(panel);
	}

	@Override
	protected Waiter getWaiter() {
		
		return new Waiter() {

			private boolean isSetup = false;
			
			@Override
			protected int getIntervalMs() {
				return 1000;
			}

			@Override
			protected String getWaitingText() {
				return "Waiting for USB network";
			}

			@Override
			protected String getReadyText() {
				return "USB Network detected";
			}

			@Override
			protected boolean performWait() {
				String usb = model.getUsbNetworkInterface();
				if (usb != null) {
					if (!isSetup) {
						model.setupNetwork();
						isSetup = true;
					}
					return true;
				}
				return false;
			}
		};
	}

	@Override
	protected View getNextView() {
		return new FirmwareFlashConfirmView(model, controller);
	}
}
