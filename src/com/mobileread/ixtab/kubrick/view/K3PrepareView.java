package com.mobileread.ixtab.kubrick.view;

import com.googlecode.lanterna.gui.Action;
import com.googlecode.lanterna.gui.component.Label;
import com.googlecode.lanterna.gui.component.Panel;
import com.googlecode.lanterna.terminal.Terminal.Color;
import com.mobileread.ixtab.kubrick.controller.Controller;
import com.mobileread.ixtab.kubrick.controller.Controller.ButtonType;
import com.mobileread.ixtab.kubrick.lanterna.MaxVerticalLayout;
import com.mobileread.ixtab.kubrick.model.Model;

public class K3PrepareView extends View {

	protected K3PrepareView(Model model, Controller controller) {
		super(model, controller);
	}

	@Override
	public void display(Panel panel) {
		controller.setTitle("Put your Kindle into USB downloader mode", Color.BLACK);
		panel.setLayoutManager(new MaxVerticalLayout(true, true));
		
		StringBuilder msg = new StringBuilder();
		// MAX LENGTH: #####################################################################
		msg.append("You will now put your device into USB downloader mode.\n\n");
		msg.append("1. Connect the Kindle to your Computer\n");
		msg.append("2. Slide and hold the Kindle's power slider, and keep it pressed for\n");
		msg.append("   AT LEAST 30 seconds.\n");
		msg.append("3. Keep the power slider \"pressed\", and in addition, press the\n");
		msg.append("   VOLUME DOWN button.\n");
		msg.append("4. Release the power slider.\n");
		msg.append("5. Release the VOLUME down button.");
		msg.append("\n\nPress NEXT when you're done.\n\n ");
		
		panel.addComponent(new Label(msg.toString()));
		
		controller.setButtonAction(ButtonType.NEXT, new Action() {
			@Override
			public void doAction() {
				controller.setView(new K3WaitForDownloadView(model, controller));
			}
		});
		
	}

}
