package com.mobileread.ixtab.kubrick.controller;

import com.googlecode.lanterna.gui.Action;
import com.googlecode.lanterna.terminal.Terminal.Color;
import com.mobileread.ixtab.kubrick.view.View;


public interface Controller {
	
	public enum ButtonType {
		NEXT,
		CANCEL,
	};

	void setTitle(String text, Color color);
	void setView(View newView);
	void setButtonAction(ButtonType buttonType, Action action);
	Action getButtonAction(ButtonType buttonType);
	void exit();

}
