package com.mobileread.ixtab.kubrick.developer;

import java.awt.Dimension;

import javax.swing.JFrame;
import javax.swing.SwingUtilities;

import com.googlecode.lanterna.TerminalFacade;
import com.googlecode.lanterna.terminal.Terminal;
import com.googlecode.lanterna.terminal.swing.SwingTerminal;
import com.mobileread.ixtab.kubrick.Metadata;

/**
 * This class is exactly what its name says: a horrible, horrific, ugly,
 * hideous, disgusting hack.
 * 
 * It's a workaround for lanterna producing more or less random results when
 * trying to simulate a 80x25 terminal in a windowed environment.
 * 
 * As ugly as this hack is, it still works *relatively* well (at least for me,
 * that is). After initialization, which may take a second or two, the Swing
 * screen SHOULD reflect the layout of an 80x25 text terminal. It does so most
 * of the time - but not always. There is NO GUARANTEE that it will.
 * 
 * The entire thing will go mad at times (expanding or shrinking the screen too
 * much), but restarting will fix it in most cases. If restarting doesn't fix
 * it, see above: this is a horrible hack, and is does not claim to be anywhere
 * near "usable". Try to fiddle with the sleep intervals, that should help.
 * 
 * Your mileage may vary significantly. Don't blame me. You have been told.
 * 
 * The grand conclusion is: don't rely on what you see in the GUI. Have it
 * run in a TUI, and adjust the code accordingly.
 * 
 */
class HorribleSwingTerminalHack extends Thread {

	public static Terminal instantiateTerminal() {
		SwingTerminal term = TerminalFacade.createSwingTerminal(INITIAL_WIDTH,
				INITIAL_HEIGHT);
		new HorribleSwingTerminalHack(term).start();
		return term;
	}

	private static final int INITIAL_WIDTH = 83;
	private static final int INITIAL_HEIGHT = 27;

	private static final int MINIMUM_WIDTH = 79;
	private static final int MINIMUM_HEIGHT = 22;

	private static final int TARGET_WIDTH = 80;
	private static final int TARGET_HEIGHT = 25;

	private static final int INCREASE = 1;
	private static final int DECREASE = -1;

	private static final int RESIZE_STEP_PX = 10;

	private final SwingTerminal term;

	/*
	 * More is not necessarily better. 10 ms seems to be a reasonable delay on my computer.
	 */
	private final long sleepMs = 10;
	
	private final Dimension dimBuffer = new Dimension();

	private enum Stage {
		WAIT_FRAME, MINIMIZE_HEIGHT, MINIMIZE_WIDTH, RESTORE_HEIGHT, RESTORE_WIDTH, DONE, FINISHED,
	}

	private Stage stage = Stage.WAIT_FRAME;
	private JFrame frame = null;

	public HorribleSwingTerminalHack(SwingTerminal term) {
		super();
		this.term = term;
		this.setDaemon(true);
	}

	@Override
	public void run() {
		final Object[] result = new Object[1];
		while (stage != Stage.FINISHED) {
			synchronized (result) {
				try {
					Thread.sleep(sleepMs);
					Runnable run = new Runnable() {
						@Override
						public void run() {
							try {
								runLoopInUIThread();
							} catch (Throwable t) {
								result[0] = t;
							} finally {
								synchronized (result) {
									result.notify();
								}
							}
						}
					};
					SwingUtilities.invokeLater(run);
					result.wait();
					if (result[0] instanceof Throwable) {
						throw (Throwable) result[0];
					}
				} catch (InterruptedException ie) {
					System.err.println("unexpected exception: "
							+ ie.getMessage());
					stage = Stage.FINISHED;
				} catch (Throwable t) {
					// even more unexpected :-D
					t.printStackTrace();
					stage = Stage.FINISHED;
				}
			}
		}
	}

	private void runLoopInUIThread() throws Throwable {
		switch (stage) {
		case DONE:
			frame.setTitle(Metadata.getProductWithVersion() + " (DEBUG)");
			frame.pack();
			frame.setResizable(false);
			stage = Stage.FINISHED;
			break;
		case WAIT_FRAME:
			if (waitForFrame()) {
				stage = Stage.MINIMIZE_HEIGHT;
			}
			break;
		case MINIMIZE_HEIGHT:
			if (adjustHeight(DECREASE, MINIMUM_HEIGHT)) {
				stage = Stage.RESTORE_HEIGHT;
			}
			break;
		case RESTORE_HEIGHT:
			if (adjustHeight(INCREASE, TARGET_HEIGHT)) {
				stage = Stage.MINIMIZE_WIDTH;
			}
			break;
		case MINIMIZE_WIDTH:
			if (adjustWidth(DECREASE, MINIMUM_WIDTH)) {
				stage = Stage.RESTORE_WIDTH;
			}
			break;
		case RESTORE_WIDTH:
			if (adjustWidth(INCREASE, TARGET_WIDTH)) {
				stage = Stage.DONE;
			}
			break;
		}
	}

	private boolean waitForFrame() {
		frame = term.getJFrame();
		if (frame != null) {
			frame.setTitle(Metadata.getProductWithVersion() + " (INITIALIZING)");
			return true;
		}
		return false;
	}

	private boolean adjustHeight(int direction, int target) throws Throwable {
		int step = direction * RESIZE_STEP_PX;
		frame.getSize(dimBuffer);
		dimBuffer.height = Math.max(0, dimBuffer.height + step);
		frame.setSize(dimBuffer);
		Thread.sleep(sleepMs);
		return term.getTerminalSize().getRows() == target;
	}

	private boolean adjustWidth(int direction, int target) throws Throwable {
		int step = direction * RESIZE_STEP_PX;
		frame.getSize(dimBuffer);
		dimBuffer.width = Math.max(0, dimBuffer.width + step);
		frame.setSize(dimBuffer);
		Thread.sleep(sleepMs);
		return term.getTerminalSize().getColumns() == target;
	}

}
