package com.mobileread.ixtab.kubrick.developer;

import com.googlecode.lanterna.terminal.Terminal;
import com.mobileread.ixtab.kubrick.controller.Controller;
import com.mobileread.ixtab.kubrick.model.DebrickConfiguration;
import com.mobileread.ixtab.kubrick.model.Model;
import com.mobileread.ixtab.kubrick.view.View;

/* this class is really only meant for development, to simulate
 various behaviors.
 IMPORTANT: Because of some quirks in lanterna, you MUST make sure that in GUI developer mode,
 the LAST line in the main display area ("between the title and the buttons") remains empty.
 Otherwise, the last line WON'T be displayed in an actual production environment (a real computer)
 */

public class DeveloperMode {

	/** define this system property (to whatever you want) to enable debug mode */
	private static final String _DEBUG_PROPERTY = "kubrick.debug";

	public static final boolean DEBUGMODE = System.getProperty(DeveloperMode._DEBUG_PROPERTY, null) != null;
	public static final boolean TEXTMODE = "true".equalsIgnoreCase(System.getProperty(
			"java.awt.headless", "false"));
	
	public static boolean isEnabled() {
		return !TEXTMODE || DEBUGMODE;
	}
	
	private DeveloperMode() {
	}

	public static Terminal getSwingTerminal() {
		return HorribleSwingTerminalHack.instantiateTerminal();
	}

	public static DebrickConfiguration getConfiguration() {
		return DebrickConfiguration.get(DebrickConfiguration.K5);
	}

	public static boolean canBlastMntUs() {
		return true;
	}

	public static boolean canTTSFilesBeInstalled() {
		return true;
	}

	// return null if you don't want to influence that part of the behavior
	public static View getInitialView(Model model, Controller controller) {
		return null; //new OtherActivitiesSelectView(model, controller);
	}
}
