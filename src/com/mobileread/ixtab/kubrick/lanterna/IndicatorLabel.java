package com.mobileread.ixtab.kubrick.lanterna;
import com.googlecode.lanterna.gui.Theme.Category;
import com.googlecode.lanterna.gui.component.Label;
import com.googlecode.lanterna.terminal.Terminal.Color;

public class IndicatorLabel extends Label {
	private static final int WIDTH = 72;
	public IndicatorLabel() {
		super("", 72, Color.WHITE, true);
		setAlignment(Alignment.CENTER);
		setStyle(Category.PROGRESS_BAR_REMAINING);
	}
	
	@Override
	public void setText(String text) {
		int fill = (WIDTH - text.length()) / 2;
		StringBuilder centered = new StringBuilder();
		for (int i = 0; i < fill; ++i) {
			centered.append(" ");
		}
		centered.append(text);
		for (int i = 0; i < fill; ++i) {
			centered.append(" ");
		}
		// this should happen at most once, due to rounding
		while (centered.length() < WIDTH) {
			centered.append(" ");
		}
		super.setText(centered.toString());
	}
	
	public void setCompleted(boolean completed) {
		setStyle(completed ? Category.PROGRESS_BAR_COMPLETED : Category.PROGRESS_BAR_REMAINING);
	}

}
