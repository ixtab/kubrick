package com.mobileread.ixtab.kubrick.lanterna;

import com.googlecode.lanterna.gui.layout.HorisontalLayout;

public class MaxHorizontalLayout extends HorisontalLayout {
	
	private final boolean vertically;
	private final boolean horizontally;

	public MaxHorizontalLayout(boolean vertically, boolean horizontally) {
		super();
		this.vertically = vertically;
		this.horizontally = horizontally;
	}

	@Override
	public boolean maximisesVertically() {
		return vertically;
	}

	@Override
	public boolean maximisesHorisontally() {
		return horizontally;
	}

}
