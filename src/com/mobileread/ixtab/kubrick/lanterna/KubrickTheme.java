package com.mobileread.ixtab.kubrick.lanterna;

import com.googlecode.lanterna.gui.GUIScreenBackgroundRenderer;
import com.googlecode.lanterna.gui.TextGraphics;
import com.googlecode.lanterna.gui.Theme;
import com.googlecode.lanterna.screen.ScreenCharacterStyle;
import com.googlecode.lanterna.terminal.Terminal.Color;
import com.googlecode.lanterna.terminal.TerminalPosition;
import com.googlecode.lanterna.terminal.TerminalSize;
import com.mobileread.ixtab.kubrick.Metadata;
import com.mobileread.ixtab.kubrick.util.Strings;
import com.mobileread.ixtab.kubrick.developer.DeveloperMode;

public class KubrickTheme extends Theme implements GUIScreenBackgroundRenderer {

	public KubrickTheme() {
		super();
		// we don't use progress bars, and we can't extend the Definition enum,
		// so we just abuse the existing definitions.
		setDefinition(Category.PROGRESS_BAR_COMPLETED, new Definition(
				Color.WHITE, Color.GREEN, false));
		setDefinition(Category.PROGRESS_BAR_REMAINING, new Definition(
				Color.WHITE, Color.RED, false));
	}

	@Override
	public void drawBackground(TextGraphics textGraphics) {
		textGraphics.applyTheme(Theme.Category.SCREEN_BACKGROUND);
		textGraphics.fillRectangle(' ', new TerminalPosition(0, 0),
				textGraphics.getSize());
		
		textGraphics.drawString(getBackgroundInset(), 0, getTitle(textGraphics), ScreenCharacterStyle.Bold);
	}

	public int getBackgroundInset() {
		return 1;
	}
	
	private String cachedTitle = null;
	String getTitle(TextGraphics g) {
		if (DeveloperMode.isEnabled() || cachedTitle == null) {
			TerminalSize size = g.getSize();
			int inset = getBackgroundInset();
			
			try {
				cachedTitle = Strings.spaceEvenly(size.getColumns()-inset*2, Metadata.getProductWithVersion(), Metadata.getInfo(), Metadata.getAuthor());
			} catch (Throwable t) {
				// yes, this happens, especially in Swing (GUI) mode
				return "";
			}
		}
		return cachedTitle;
	}
}
