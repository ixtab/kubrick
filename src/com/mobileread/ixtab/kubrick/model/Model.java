package com.mobileread.ixtab.kubrick.model;

import java.util.HashSet;
import java.util.List;
import java.util.Set;
import java.util.TreeSet;

import com.googlecode.lanterna.gui.GUIScreen;
import com.mobileread.ixtab.kubrick.util.Executables;
import com.mobileread.ixtab.kubrick.developer.DeveloperMode;

public class Model {
	
	public final GUIScreen owner;
	
	private DebrickConfiguration configuration = null;
	private Set<String> disconnectedNetworks = null;
	
	public Model(GUIScreen owner) {
		this.owner = owner;
		if (DeveloperMode.isEnabled()) {
			configuration = DeveloperMode.getConfiguration();
		}
	}

	public List<DebrickConfiguration> getAvailableConfigurations() {
		return DebrickConfiguration.AVAILABLE_CONFIGURATIONS;
	}

	public void setConfiguration(DebrickConfiguration configuration) {
		this.configuration = configuration;
	}

	public void setDisconnectedUsbNetworks() {
		List<String> usbs = Executables.listUsbNetworks();
		disconnectedNetworks = new TreeSet<String>(usbs);
	}
	
	public DebrickConfiguration getConfiguration() {
		return configuration;
	}
	
	public boolean isDisconnectedUsbNetworksSet() {
		return disconnectedNetworks != null;
	}

	public boolean isUsbDownloaderDevicePresent() {
		List<String> devices = Executables.listUsbDownloaderDevices();
		return devices != null && devices.size() == 1;
	}

	public boolean isFastbootDevicePresent() {
		List<String> devices = Executables.listFastbootDevices();
		return devices != null && devices.size() == 1;
	}

	public void enterFastbootMode() {
		Executables.enterFastbootMode();
	}

	public void flashDiags() {
		Executables.flashDiags(getConfiguration().id);
	}

	public void setupNetwork() {
		String usb = getUsbNetworkInterface();
		if (usb == null) {
			throw new RuntimeException("No valid network interface");
		}
		Executables.setNetworkInterface(usb);
	}
	
	public void flashMain() {
		Executables.flashMain(getConfiguration().id);
	}
	
	public String getUsbNetworkInterface() {
		List<String> usbs = Executables.listUsbNetworks();
		Set<String> pristine = disconnectedNetworks;
		
		// really only for testing. It will be set in normal use.
		if (pristine == null) {
			pristine = new HashSet<String>();
		}
		
		String newNet = null;
		for (String usbNet: usbs) {
			if (!pristine.contains(usbNet)) {
				newNet = usbNet;
				break;
			}
		}
		return newNet;
	}

	public void installJailbreak() {
		Executables.installJailbreak();
	}

	public void fixYourKindleNeedsRepair() {
		Executables.fixYourKindleNeedsRepair();
	}

	public void blastVarLocal() {
		Executables.blastVarLocal();
	}

	public void blastMntUs() {
		Executables.blastMntUs();
	}

	public boolean canBlastMntUs() {
		return DeveloperMode.isEnabled() ? DeveloperMode.canBlastMntUs() : Executables.canBlastMntUs();
	}
	
	public boolean canTTSFilesBeInstalled() {
		return DeveloperMode.isEnabled() ? DeveloperMode.canTTSFilesBeInstalled() : Executables.canTTSFilesBeInstalled();
	}

	public void reinstallTTSFiles() {
		Executables.reinstallTTSFiles();
	}

	public void rebootToMain() {
		Executables.rebootToMain();
	}

	public void shutdown() {
		if (DeveloperMode.isEnabled()) {
			System.exit(0);
		} else {
			Executables.shutdown();
		}
	}

	public void waitForK3USBDownloaderMode() {
		Executables.waitForK3USBDownloaderMode();
	}
	
	public void flashK3Kernel() {
		Executables.flashK3Kernel();
	}

	public Process flashK3Main() {
		return Executables.flashK3Main();
	}

}
