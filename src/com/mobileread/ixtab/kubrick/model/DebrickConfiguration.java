package com.mobileread.ixtab.kubrick.model;

import java.io.File;
import java.util.ArrayList;
import java.util.List;

public class DebrickConfiguration {
	public static final String K3 = "k3";
	public static final String K4 = "k4";
	public static final String K5 = "k5";

	private static final int FLAG_NONE = 0;
	private static final int FLAG_K3FLASHER = 1;
	private static final int FLAG_TTS = 2;

	private final int flags;
	public final String id;
	public final String description;
	public final String magicKey;

	// Note: the ID *MUST* match the relative directory filename!
	public DebrickConfiguration(String id, String description, String magicKey,
			int flags) {
		super();
		this.id = id;
		this.description = description;
		this.magicKey = magicKey;
		this.flags = flags;
	}

	@Override
	public String toString() {
		return description;
	}

	static final List<DebrickConfiguration> AVAILABLE_CONFIGURATIONS;

	static {
		AVAILABLE_CONFIGURATIONS = new ArrayList<DebrickConfiguration>();
		AVAILABLE_CONFIGURATIONS
				.add(new DebrickConfiguration(
						K3,
						"Kindle 3 (Keyboard)      3.4                   MANDATORY         ",
						null, FLAG_K3FLASHER));
		AVAILABLE_CONFIGURATIONS
				.add(new DebrickConfiguration(
						K4,
						"Kindle 4 (Non-Touch)     4.1.1                 OPTIONAL          ",
						"five-way DOWN button", FLAG_NONE));
		AVAILABLE_CONFIGURATIONS
				.add(new DebrickConfiguration(
						K5,
						"Kindle 5 (Touch)         5.1.2                 OPTIONAL          ",
						"HOME button", FLAG_TTS));
	}

	public static DebrickConfiguration get(String id) {
		for (DebrickConfiguration conf : AVAILABLE_CONFIGURATIONS) {
			if (conf.id == id) {
				return conf;
			}
		}
		throw new IllegalArgumentException(id + " is not a valid configuration");
	}

	public boolean isAvailable() {
		File dir = new File("resources/devices/" + id);
		return dir.exists();
	}

	public boolean usesK3Flasher() {
		return (flags & FLAG_K3FLASHER) == FLAG_K3FLASHER;
	}

	public boolean hasTTS() {
		return (flags & FLAG_TTS) == FLAG_TTS;
	}

}
