package com.mobileread.ixtab.kubrick.util;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.util.ArrayList;
import java.util.List;

public final class Executor {
	
	// returns <tt>null</tt> if the execution failed, otherwise the output of the command
	public static List<String> execute(String... cmd) {
		int[] ret = new int[1];
		List<String> result = execute(ret, cmd);
		return ret[0] != 0 ? null : result; 
	}
	
	// returnValue is used as a "pseudo-pointer" to an int. Oh, and we don't care about
	// streaming here, at all.
	public static List<String> execute(int[] returnValue, String... cmdArray) {
		try {
			InputStream is =  execStream(returnValue, cmdArray);
			return asStringList(is);
		} catch(Exception ex) {}
		{
			return null;
		}
	}
	
	
	
	private static List<String> asStringList(InputStream is) throws IOException {
		List<String> result = new ArrayList<String>();
		BufferedReader br = new BufferedReader(new InputStreamReader(is));
		for (String line = br.readLine(); line != null; line = br.readLine()) {
			result.add(line);
		}
		return result;
	}

	public static InputStream execStream(int[] returnValue, String[] cmdArray) throws Exception {
		Process p = Runtime.getRuntime().exec(cmdArray);
		returnValue[0] = p.waitFor();
		return p.getInputStream();
	}
	
	public static Process executeProcess(String... cmd)  {
		try {
			return Runtime.getRuntime().exec(cmd);
		} catch (Exception e) {
			return null;
		}
	}
}
