package com.mobileread.ixtab.kubrick.util;

import java.util.List;

public class Executables {
	public static List<String> listUsbNetworks() {
		return Executor.execute("/bin/sh", "resources/scripts/local/lsusbnets.sh");
	}

	public static List<String> listUsbDownloaderDevices() {
		return Executor.execute("/bin/sh", "resources/scripts/local/lsimxusbdevs.sh");
	}

	public static List<String> listFastbootDevices() {
		return Executor.execute("/bin/sh", "resources/scripts/local/lsfastbootdevs.sh");
	}

	private static void executeOrFail(String... cmd) {
		int[] retval = new int[1];
		Executor.execute(retval, cmd);
		if (retval[0] != 0) {
			throw new RuntimeException("command failed");
		}
	}
	
	public static void enterFastbootMode() {
		executeOrFail("/bin/sh", "./imx_usb.sh", "resources/imx_usb_img/fastboot.bin");
	}

	public static void flashDiags(String model) {
		executeOrFail("/bin/sh", "resources/scripts/local/flash_diags.sh", model);
	}

	public static void flashMain(String model) {
		executeOrFail("/bin/sh", "resources/scripts/local/flash_main.sh", model);
	}

	public static void setNetworkInterface(String usb) {
		executeOrFail("/bin/sh", "resources/scripts/local/prepare_network.sh", usb);
	}

	public static void installJailbreak() {
		executeOrFail("/bin/sh", "resources/scripts/local/install_jailbreak.sh");
	}

	public static void fixYourKindleNeedsRepair() {
		executeOrFail("/bin/sh", "resources/scripts/local/fix_kindle_needs_repair.sh");
	}

	public static void blastVarLocal() {
		executeOrFail("/bin/sh", "resources/scripts/local/blast_var_local.sh");
	}

	public static void blastMntUs() {
		executeOrFail("/bin/sh", "resources/scripts/local/blast_mnt_us.sh");
	}

	public static boolean canBlastMntUs() {
		return doesScriptReturnOk("resources/scripts/local/check_blast_mnt_us.sh");
	}
	
	public static void reinstallTTSFiles() {
		executeOrFail("/bin/sh", "resources/scripts/local/install_tts.sh");
	}
	
	public static boolean canTTSFilesBeInstalled() {
		return doesScriptReturnOk("resources/scripts/local/check_tts.sh");
	}

	private static boolean doesScriptReturnOk(String shellScript) {
		try {
			List<String> lines = Executor.execute("/bin/sh", shellScript);
			for (String line: lines) {
				if (line.startsWith("OK:")) {
					return true;
				}
			}
			return false;
		} catch (Throwable t) {
			return false;
		}
	}

	public static void rebootToMain() {
		executeOrFail("/bin/sh", "resources/scripts/local/reboot_main.sh");
	}

	public static void shutdown() {
		Executor.execute("/sbin/poweroff");
	}

	public static void waitForK3USBDownloaderMode() {
		executeOrFail("/bin/sh", "resources/scripts/local/k3_waitfor.sh");
	}
	
	public static void flashK3Kernel() {
		executeOrFail("/bin/sh", "resources/scripts/local/k3_flash_kernel.sh");
	}
	
	public static Process flashK3Main() {
		return Executor.executeProcess("/bin/sh", "resources/scripts/local/k3_flash_main.sh");
	}

}
