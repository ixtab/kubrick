package com.mobileread.ixtab.kubrick.util;

public class Strings {
	private Strings() {};
	
	public static String spaceEvenly(int maxLength, String... parts) {
		if (parts == null || parts.length == 0) {
			return "";
		}
		if (parts.length == 1) {
			return parts[0];
		}
		
		int eachGap = 1;
		
		int totalLength = getTotalLength(parts);
		int totalGaps = maxLength - totalLength;
		
		if (totalGaps <= parts.length -1 ) {
			return spaceEvenly(maxLength, eachGap, parts);
		}
		
		if (parts.length % 2 == 1) {
			// odd number of parts. Equalize the leftmost and rightmost one, so the middle one ends up centered
			String left = parts[0];
			String right = parts[parts.length-1];
			int leftLength = left.length();
			int rightLength = right.length();
			
			if (leftLength < rightLength) {
				parts[0] = appendSpacesUntilLength(left, rightLength);
			} else if (rightLength < leftLength) {
				parts[parts.length-1] = prependSpacesUntilLength(right, leftLength);
			}
		}
		
		totalLength = getTotalLength(parts);
		totalGaps = maxLength - totalLength;
		
		if (totalGaps <= parts.length -1 ) {
			return spaceEvenly(maxLength, eachGap, parts);
		}
		
		eachGap = totalGaps / (parts.length - 1);

		return spaceEvenly(maxLength, eachGap, parts);
	}

	private static String prependSpacesUntilLength(String right, int leftLength) {
		return new String(getSpaces(leftLength - right.length())).concat(right);
	}

	private static String appendSpacesUntilLength(String left, int rightLength) {
		return left.concat(new String(getSpaces(rightLength - left.length())));
	}

	private static String spaceEvenly(int maxLength, int eachGap, String[] parts) {
		String result = join(parts, eachGap);
		result = result.substring(0, Math.min(result.length(), maxLength));
		return result;
	}

	private static int getTotalLength(String[] parts) {
		int total = 0;
		for (String part: parts) {
			total += part.length();
		}
		return total;
	}
	
	private static String join(String[] parts, int spaces) {
		char[] space = getSpaces(spaces);
		
		StringBuilder sb = new StringBuilder(parts[0]);
		for (int i =1; i < parts.length; ++i) {
			sb.append(space);
			sb.append(parts[i]);
		}
		return sb.toString();
	}

	private static char[] getSpaces(int count) {
		char[] space = new char[count];
		for (int i=0; i < count; ++i) {
			space[i] = ' ';
		}
		return space;
	}

}
