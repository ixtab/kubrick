package com.mobileread.ixtab.kubrick;

public final class Metadata {
	private static final String PRODUCT = "Kubrick";
	private static final String VERSION = "3.6";
	private static final String AUTHOR ="ixtab";
	private static final String INFO = "mobileread.com";
	
	
	public static String getProduct() {
		return PRODUCT;
	}
	
	public static String getVersion() {
		return VERSION;
	}
	
	public static String getProductWithVersion() {
		return getProduct() + " " + getVersion();
	}

	public static String getAuthor() {
		return AUTHOR;
	}

	public static String getInfo() {
		return INFO;
	}
	
	
}
