#!/bin/sh -x

# Standard screen setup. This should really only be relevant on Mac computers
# (which have a higher, unfortunately too high, setting, so that text extends
# below the bottom of the screen. Not nice.)
/bin/stty rows 25
/bin/stty cols 80

# If on a different tty than tty1, assume that the user knows what he's doing
if [ `tty` != "/dev/tty1" ]; then
	return
else
	clear
fi

# file names
KUBRICK_MARKER=boot/KUBRICK_USBSTICK_DO_NOT_DELETE.txt
BOOT_ISO=efi/boot/boot.iso
KUBRICK_DIR=/media/cdrom/kubrick
KUBRICK_SH=$KUBRICK_DIR/kubrick.sh
JAVA_SFS=/media/cdrom/kubrick/java.sfs

# Check if requirements are mounted
if [ -f "$KUBRICK_SH" ]; then
	# do nothing
	echo -n ""
else
	# Disable kernel messages (printed to tty1 be default, which messes up
	# the display)
	# stolen from http://unix.stackexchange.com/questions/44999/how-can-i-hide-messages-of-udev/45525#45525
	# (and slightly modified)
	echo "0 0 0 0" > /proc/sys/kernel/printk

	# Disable "screen saver"
	/usr/bin/setterm -blank 0 -powersave off -powerdown 0

	# create required directory
	mkdir -p /mnt/java

	# Revert to pristine state
	(mount | grep /mnt/java >/dev/null) && umount /mnt/java
	(mount | grep /media/cdrom >/dev/null) && umount /media/cdrom

	# If booted from a USB stick, determine the device, and try to mount the contained ISO image
	STICK=""
	if [ `grep -c kubrick-stick /proc/cmdline` == 1 ]; then
		echo "Kubrick was booted from a USB stick, determining device..."
		for dev in `ls /dev/hd? /dev/sd?`; do
			for part in `/sbin/fdisk -l $dev 2>/dev/null | grep FAT32 | awk '{print $1}'`; do
				(mount | grep /media/usbdisk >/dev/null) && umount /media/usbdisk
				if mount -o ro $part /media/usbdisk 2>/dev/null; then
					if [ ! -f "/media/usbdisk/$KUBRICK_MARKER" ]; then
						echo "$part does not contain Kubrick marker file, ignoring partition"
						continue;
					fi
					if [ ! -f "/media/usbdisk/$BOOT_ISO" ]; then
						echo "$part contains a Kubrick marker file, but no"
						echo "$BOOT_ISO file. This is not normal".
						continue;
					else
						STICK=$part
						break 2
					fi
				else
					echo "Problem mounting $part, ignoring that partition"
					continue
				fi
			done
		done
		if [ "x$STICK" = "x" ]; then
			(mount | grep /media/usbdisk >/dev/null) && umount /media/usbdisk
			echo " "
			echo "No usable Kubrick partition found. This is not supposed to happen."
			echo "If you modified the USB disk you installed Kubrick on, make sure"
			echo "that you didn't delete vital files from the \"boot\" directory."
			echo " "
			echo "We'll now continue and try to find a Kubrick CD or DVD."
			echo " "
		else
			echo "Kubrick USB stick is on partition: $STICK, mounting ISO image..."
			mount -o loop,ro /media/usbdisk/$BOOT_ISO /media/cdrom
		fi
	fi

	# try to determine CD or DVD drive, if nothing was mounted from USB yet
	if [ ! -f $KUBRICK_SH ]; then
		echo "Determining CD/DVD device..."
		for cdrom in `cat /proc/sys/dev/cdrom/info | grep 'drive name' | cut -f3-`; do
			# echo "Trying: /dev/$cdrom";
			(mount | grep /media/cdrom >/dev/null) && umount /media/cdrom
			if mount -o ro /dev/$cdrom /media/cdrom 2>/dev/null; then
				echo "Mounted CD from /dev/$cdrom"
				if [ -f $JAVA_SFS -a -f $KUBRICK_SH ]; then
					# echo "/dev/$cdrom seems to contain the Kubrick files... Good!"
					break
				else
					echo "/dev/$cdrom does not contain all required files"
				fi
			else
				echo "Failed to mount /dev/$cdrom - probably because no media inserted"
			fi
		done
	fi

	if [ -f $JAVA_SFS ]; then
		echo "Mounting $JAVA_SFS"
		(mount | grep /mnt/java >/dev/null) && umount /mnt/java
		mount -o loop -t squashfs $JAVA_SFS /mnt/java >/dev/null 2>&1
	else
		echo "Missing required Java files. Kubrick will not work."
	fi
fi

FAILED=1
if [ -f $KUBRICK_SH ]; then
	if [ -f /mnt/java/bin/java ]; then
		if [ `echo "$PATH" | grep -c /mnt/java/bin` == 0 ]; then
			export PATH="$PATH:/mnt/java/bin"
		fi
		echo "Auto-starting the Kubrick Wizard..."
		cd $KUBRICK_DIR
		sh ./kubrick.sh
		FAILED=0
	else
		echo " "
		echo "The Java binaries do not seem to be available."
		echo "This is an unexpected situation."
		echo " "
	fi
fi

if [ $FAILED == 1 ]; then
	echo "####################################################"
	echo "#                      ERROR                       #"
	echo "####################################################"
	echo "#                                                  #"
	echo "# The Kubrick Wizard could not be started. Sorry.  #"
	echo "#                                                  #"
	echo "# If you have some Linux experience, you may want  #"
	echo "# to troubleshoot the problem by looking at        #"
	echo "# /root/autostart.sh and determining what could be #"
	echo "# causing this.                                    #"
	echo "#                                                  #"
	echo "# Otherwise, please type \"poweroff\" (without the   #"
	echo "# quotes) to shut down the system.                 #"
	echo "####################################################"
	cd $HOME
fi
