This project only contains the UI interface of the Kubrick live system.

The entire Kubrick CD requires more than this. It was created ad-hoc,
and can't be easily "reproduced" - not even by me.

If you want to modify the Kubrick CD, your best bet is to
run "slitaz-installer" from the CD, and to follow the instructions
in http://doc.slitaz.org/en:handbook:hacklivecd




==========================================================================

A few notes for myself (and maybe for you, too, but only if you understand them) ;)
To create a Live USB stick from a CD booted in a VM with the correct
HD size (I use 1,000,000,000 bytes =~ 970 MB), do the following:

**************************************************************************
NOTE: ***** AT YOUR OWN RISK!!!!! *****
Don't blame me if you got this wrong and erased your hard drive!!! And,
just in case, a few more exclamation points. You really want to understand
what you are doing before actually doing it !!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
**************************************************************************

1. dd bs=4096 if=/dev/zero of=/dev/sda1
2. tazpkg get-install dosfstools
3. mkdosfs -n Kubrick -F 32 /dev/sda1
4. tazusb gen-liveusb /dev/sda1
5. mkdir /tmp/sda1
6. mount /dev/sda1 /tmp/sda1
7. cp -r /media/cdrom/kubrick /tmp/sda1/
8. cp /media/cdrom/java.sfs /tmp/sda1/
9. umount /tmp/sda1
10. # /dev/sda now IS kubrick.img. Use dd  - I use it in conjunction with nc, like so:
  10.1 (on the host): nc -l 31337 | cstream -T 5 -o kubrick.img
  10.2 (in the VM): dd if=/dev/sda|nc 10.0.2.2 31337
  
